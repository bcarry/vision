# Class for Targets

from astropy.coordinates import SkyCoord, AltAz
import astropy.units as u
from astropy.time import Time

import vision


# --------------------------------------------------------------------------------
class ListOfTargets:

    # --------------------------------------------------------------------------------
    def __init__(self, targets):
        # self.targets = [Target(t) for t in targets]
        self.targets = targets

    # --------------------------------------------------------------------------------
    # Getters
    @property
    def targets(self):
        return [t.identifier for t in self._targets]

    @property
    def names(self):
        return [t.name for t in self._targets]

    @property
    def identifiers(self):
        return [t.identifier for t in self._targets]

    # Return a single target from its name
    def target(self, target):
        for t in self._targets:
            if t.identifier == target:
                return t

    # --------------------------------------------------------------------------------
    # Setters
    @targets.setter
    def targets(self, value):
        self._targets = value

    # --------------------------------------------------------------------------------
    def copy(self):
        from copy import deepcopy

        return deepcopy(self)


# --------------------------------------------------------------------------------
class Target:

    def __name__(self):
        return "Target"

    # --------------------------------------------------------------------------------
    def __init__(
        self,
        identifier=None,
        coordinates=None,
        asteroid=None,
        comet=None,
        satellite=None,
        planet=None,
        cds=None,
        resolve=True,
    ):

        # Identification
        self.identifier = identifier
        self.type = None
        self.number = None
        self.name = None
        self.label = None

        # General properties
        self.epoch = None
        self.coordinates = None
        self.ra = None
        self.dec = None
        self.glon = None
        self.glat = None
        self.mag = None
        self.rise = None
        self.transit = None
        self.set = None
        self.solar_elongation = None
        self.lunar_elongation = None

        # Solar System specifics
        self.phase = None
        self.rate = None
        self.range = None
        self.heliodist = None
        self.diameter = None

        # Derived properties
        self.altaz = None
        self.duration = None
        self.is_visible = False
        self.start = None
        self.end = None

        # Plotting properties
        self.marker = None
        self.color = None
        self.size = None
        self.alpha = None
        self.linestyle = None

        if "identifier" in locals():
            if isinstance(identifier, str):

                split = identifier.split(":")
                if len(split) == 2:
                    match split[0]:

                        # Solar System Object
                        case "a":
                            self.set_asteroid(split[1], resolve=resolve)
                        case "p":
                            self.set_planet(split[1])
                        case "c":
                            self.set_comet(split[1])
                        case "s":
                            self.set_satellite(split[1])

                        # SIMBAD object
                        case "cds":
                            self.from_cds(split[1])

                        # Coordinates
                        case "eq":
                            self.from_coordinates(split[1])

                        # Else
                        case _:
                            print("Invalid prefix")
            elif isinstance(identifier, dict):
                self.from_dict(identifier)

        # Resolve object
        if coordinates is not None:
            self.from_coordinates(coordinates)

        if cds is not None:
            self.from_cds(cds, resolve=resolve)

        if asteroid is not None:
            self.set_asteroid(asteroid)
        if planet is not None:
            self.set_planet(planet)
        if comet is not None:
            self.set_comet(comet)
        if satellite is not None:
            self.set_satellite(satellite)

    # --------------------------------------------------------------------------------
    def copy(self):
        """Full copy of the Target"""
        from copy import deepcopy

        return deepcopy(self)

    # --------------------------------------------------------------------------------
    # Retrieve target information from a dictionary (from ViSiON json)
    def from_dict(self, dic):
        """Create a Target object from a dictionary (ViSiON API)"""

        # Reference epoch for the properties
        if "epoch" in dic.keys():
            self.epoch = dic["epoch"]

        # Coordinates and magnitude
        if ("ra" in dic.keys()) and ("dec" in dic.keys()):
            self.ra = dic["ra"]
            self.dec = dic["dec"]
            self.coordinates = SkyCoord(ra=dic["ra"], dec=dic["dec"], unit=(u.deg, u.deg))
        if "galactic" in dic.keys():
            self.glon = dic["galactic"]["longitude"]
            self.glat = dic["galactic"]["latitude"]
        if "magnitude" in dic.keys():
            self.mag = dic["magnitude"]

        # Rise Transit Set
        if "rise" in dic.keys():
            self.rise = {
                "time": Time(dic["rise"][0]["time"], format="isot"),
                "azimuth": float(dic["rise"][0]["azimuth"]),
            }

        if "transit" in dic.keys():
            self.transit = {"lower": None, "upper": None}
            if "lower" in dic["transit"].keys():
                self.transit["lower"] = {
                    "time": Time(dic["transit"]["lower"][0]["time"], format="isot"),
                    "azimuth": float(dic["transit"]["lower"][0]["azimuth"]),
                }

            if "upper" in dic["transit"].keys():
                self.transit["upper"] = {
                    "time": Time(dic["transit"]["upper"][0]["time"], format="isot"),
                    "azimuth": float(dic["transit"]["upper"][0]["azimuth"]),
                }

        if "set" in dic.keys():
            self.set = {
                "time": Time(dic["set"][0]["time"], format="isot"),
                "azimuth": float(dic["set"][0]["azimuth"]),
            }

        # Elongations
        if "solar_elongation" in dic.keys():
            self.solar_elongation = dic["solar_elongation"]
        if "lunar_elongation" in dic.keys():
            self.lunar_elongation = dic["lunar_elongation"]

        # Solar System specifics
        if "phase_angle" in dic.keys():
            self.phase = dic["phase_angle"]
        if "rate_of_motion" in dic.keys():
            self.rate = dic["rate_of_motion"]
        if "obs_distance" in dic.keys():
            self.range = dic["obs_distance"]
        if "helio_distance" in dic.keys():
            self.heliodist = dic["helio_distance"]
        if "angular_diameter" in dic.keys():
            self.diameter = dic["angular_diameter"]

    # --------------------------------------------------------------------------------
    # Define target from its coordinates
    def from_coordinates(self, coordinates):
        """Create a Target object from simple RA,Dec coordinates"""
        self.type = "coordinates"

        # Check for label
        splitted = coordinates.split("=")
        if len(splitted) == 1:
            self.identifier = "EQC_" + coordinates
            self.name = coordinates
            self.label = coordinates
        else:
            self.identifier = "EQC_" + splitted[0]
            self.name = coordinates
            self.label = splitted[1]

        # Coordinates as "RA,Dec"
        s = splitted[0].split(",")
        if len(s) == 2:
            self.coordinates = SkyCoord(ra=s[0], dec=s[1], unit=(u.deg, u.deg))

        # Coordinates as "h m s +d m s"
        else:
            s = splitted[0].split(" ")
            if len(s) == 6:
                self.coordinates = SkyCoord(
                    ra=":".join(s[:3]), dec=":".join(s[3:]), unit=(u.hourangle, u.deg)
                )
            else:

                # Coordinates as "ra+-dec)"
                s = splitted[0].split("+")
                if len(s) != 2:
                    s = splitted[0].split("-")

                if len(s) == 2:
                    self.coordinates = SkyCoord(ra=s[0], dec=s[1], unit=(u.hourangle, u.deg))
                else:
                    print("Invalid coordinates")

    # --------------------------------------------------------------------------------
    # Retrieve target information from CDS
    def from_cds(self, cds, resolve=True):
        """Create a Target object by resolving identifier from CDS"""

        self.identifier = cds
        self.name = cds
        self.type = "simbad"

        if resolve:
            from astroquery.simbad import Simbad

            result = Simbad.query_object(cds)
            if not (result is None):

                self.label = result["MAIN_ID"][0]
                self.coordinates = SkyCoord(
                    ra=result["RA"], dec=result["DEC"], unit=(u.hourangle, u.deg)
                )

        else:
            self.label = cds

    # --------------------------------------------------------------------------------
    # Create a target asteroid
    def set_asteroid(self, input, resolve=True):
        """Create a Target object from asteroid identification"""

        if resolve:
            import rocks

            self.type = "asteroid"
            self.name, self.number = rocks.identify(input)
            self.identifier = input
            self.label = self.name

        else:
            self.type = "asteroid"
            self.identifier = input
            self.label = self.identifier
            if f"{input}".isnumeric():
                self.number = int(input)
            else:
                self.name = input

    # --------------------------------------------------------------------------------
    # Create a target comet
    def set_comet(self, input):
        """Create a Target object from comet identification"""

        self.type = "comet"
        self.identifier = input
        self.name = input
        self.label = input

    # --------------------------------------------------------------------------------
    # Create a target planet
    def set_planet(self, input):
        """Create a Target object from planet identification"""

        self.type = "planet"
        self.identifier = input
        self.name = input
        self.label = input

    # --------------------------------------------------------------------------------
    # Create a target satellite
    def set_satellite(self, input):
        """Create a Target object from satellite identification"""

        self.type = "satellite"
        self.identifier = input
        self.name = input
        self.label = input

    # --------------------------------------------------------------------------------
    # Update from dictionary (from ViSiON json)
    def update_from_dict(self, dic):
        """Update current Target object with a dictionary"""

        # Reference epoch for the properties
        if "epoch" in dic.keys():
            self.epoch = dic["epoch"]

        # Coordinates and magnitude
        if ("ra" in dic.keys()) and ("dec" in dic.keys()):
            self.ra = dic["ra"]
            self.dec = dic["dec"]
            self.coordinates = SkyCoord(ra=dic["ra"], dec=dic["dec"], unit=(u.deg, u.deg))
        if "galactic" in dic.keys():
            self.glon = dic["galactic"]["longitude"]
            self.glat = dic["galactic"]["latitude"]
        if "magnitude" in dic.keys():
            self.mag = dic["magnitude"]

        # Rise Transit Set
        if "rise" in dic.keys():
            self.rise = {
                "time": Time(dic["rise"][0]["time"], format="isot"),
                "azimuth": float(dic["rise"][0]["azimuth"]),
            }

        if "transit" in dic.keys():
            self.transit = {"lower": None, "upper": None}
            if "lower" in dic["transit"].keys():
                self.transit["lower"] = {
                    "time": Time(dic["transit"]["lower"][0]["time"], format="isot"),
                    "azimuth": float(dic["transit"]["lower"][0]["azimuth"]),
                }

            if "upper" in dic["transit"].keys():
                self.transit["upper"] = {
                    "time": Time(dic["transit"]["upper"][0]["time"], format="isot"),
                    "azimuth": float(dic["transit"]["upper"][0]["azimuth"]),
                }

        if "set" in dic.keys():
            # self.set = [{"time": d["time"], "azimuth": float(d["azimuth"])} for d in dic["set"]]
            self.set = {
                "time": Time(dic["set"][0]["time"], format="isot"),
                "azimuth": float(dic["set"][0]["azimuth"]),
            }

        # Elongations
        if "solar_elongation" in dic.keys():
            self.solar_elongation = dic["solar_elongation"]
        if "lunar_elongation" in dic.keys():
            self.lunar_elongation = dic["lunar_elongation"]

        if "angular_diameter" in dic.keys():
            self.diameter = dic["angular_diameter"]

        # Solar System specifics
        if "phase_angle" in dic.keys():
            self.phase = dic["phase_angle"]
        if "rate_of_motion" in dic.keys():
            self.rate = dic["rate_of_motion"]
        if "obs_distance" in dic.keys():
            self.range = dic["obs_distance"]
        if "helio_distance" in dic.keys():
            self.heliodist = dic["helio_distance"]

    # --------------------------------------------------------------------------------
    # Export object to dictionary
    def to_dict(self):
        """Export the Target object to a dictionary"""

        # Identification
        if self.identifier is not None:
            dic = {"id": self.identifier}
        else:
            dic = {}

        # Common parameters
        dic["epoch"] = self.epoch
        dic["ra"] = self.ra
        dic["dec"] = self.dec
        dic["magnitude"] = self.mag
        dic["galactic"] = {"longitude": self.glon, "latitude": self.glat}
        dic["solar_elongation"] = self.solar_elongation
        dic["lunar_elongation"] = self.lunar_elongation
        dic["duration_observability"] = self.duration

        # Solar System Specific
        if self.rate is not None:
            dic["rate_of_motion"] = self.rate
        if self.phase is not None:
            dic["phase_angle"] = self.phase
        if self.range is not None:
            dic["obs_distance"] = self.range
        if self.heliodist is not None:
            dic["helio_distance"] = self.heliodist
        if self.diameter is not None:
            dic["angular_diameter"] = self.diameter

        # Only if available
        if self.rise is not None:
            dic["rise"] = [{"time": self.rise["time"].isot, "azimuth": self.rise["azimuth"]}]
        if self.transit["lower"] is not None:
            dic["transit"] = {
                "lower": [
                    {
                        "time": self.transit["lower"]["time"].isot,
                        "azimuth": self.transit["lower"]["azimuth"],
                    }
                ]
            }
        if self.transit["upper"] is not None:
            if self.transit["lower"] is not None:
                dic["transit"]["upper"] = [
                    {
                        "time": self.transit["upper"]["time"].isot,
                        "azimuth": self.transit["upper"]["azimuth"],
                    }
                ]
            else:
                dic["transit"] = {
                    "upper": [
                        {
                            "time": self.transit["upper"]["time"].isot,
                            "azimuth": self.transit["upper"]["azimuth"],
                        }
                    ]
                }

        if self.set is not None:
            dic["set"] = [{"time": self.set["time"].isot, "azimuth": self.set["azimuth"]}]

        return dic

    # --------------------------------------------------------------------------------
    # Compute AltAz coordinates
    def compute_altaz(self, epoch=None, observer=None, **kwargs):
        """Compute the altazimutal coordinates of the target, for the given epoch and observer

        Parameters
        ----------
        epoch : vision.Epoch
            Date of observation

        observer : vision.Observer
            Observer location
        """

        # Convert observer/epoch into an AltAz grid
        if epoch.time_grid is None:
            epoch.set_time_grid(**kwargs)
        altaz = AltAz(location=observer.location, obstime=epoch.time_grid)

        # Convert Target coordinates to AltAz
        self.altaz = self.coordinates.transform_to(altaz)

    # --------------------------------------------------------------------------------
    # Check observability
    def is_observable(self, filters=None):
        """Test the observability of the target

        Parameters
        ----------
        filters : vision.Filters
            The target must satisfy these filters to be considered observable
        """

        # ----------------------------------------
        # Inputs
        if filters is None:
            print("Target > is_observable > filters not provided")
            return False

        # ----------------------------------------
        # Check General Visibility
        if not (self.mag is None):
            test_mag = (self.mag >= filters.magnitude["min"]) and (
                self.mag <= filters.magnitude["max"]
            )
        else:
            test_mag = True

        test_solar = (self.solar_elongation >= filters.solar_elongation["min"]) and (
            self.solar_elongation <= filters.solar_elongation["max"]
        )
        test_lunar = (self.lunar_elongation >= filters.lunar_elongation["min"]) and (
            self.lunar_elongation <= filters.lunar_elongation["max"]
        )

        test_gal_lat = (self.coordinates.galactic.b.deg >= filters.galactic_latitude["min"]) and (
            self.coordinates.galactic.b.deg <= filters.galactic_latitude["max"]
        )

        test_gal_lon = (self.coordinates.galactic.l.deg >= filters.galactic_longitude["min"]) and (
            self.coordinates.galactic.l.deg <= filters.galactic_longitude["max"]
        )
        test_general = test_mag and test_solar and test_lunar and test_gal_lat and test_gal_lon

        # ----------------------------------------
        # Check Visibility for Solar System Objects
        if self.type in ["planet", "asteroid", "comet", "satellite"]:
            test_phase = (self.phase >= filters.phase_angle["min"]) and (
                self.phase <= filters.phase_angle["max"]
            )
            test_rate = (self.rate >= filters.rate_of_motion["min"]) and (
                self.rate <= filters.rate_of_motion["max"]
            )
            test_range = (self.range >= filters.range_to_observer["min"]) and (
                self.range <= filters.range_to_observer["max"]
            )
            test_helio = (self.heliodist >= filters.heliocentric_distance["min"]) and (
                self.heliodist <= filters.heliocentric_distance["max"]
            )
            test_diameter = (self.diameter >= filters.angular_diameter["min"]) and (
                self.diameter <= filters.angular_diameter["max"]
            )
        else:
            test_phase = True
            test_rate = True
            test_range = True
            test_helio = True
            test_diameter = True

        test_solsys = test_phase and test_rate and test_range and test_helio and test_diameter

        # ----------------------------------------
        # Summary of observability
        self.is_visible = test_general * test_solsys

        # Set/reset visibility windows
        self.peak = {
            "epoch": None,
            "altitude": None,
            "azimuth": None,
        }
        self.start = {
            "epoch": None,
            "altitude": None,
            "azimuth": None,
        }
        self.end = {
            "epoch": None,
            "altitude": None,
            "azimuth": None,
        }

    # --------------------------------------------------------------------------------
    # Compute the observability windows
    def compute_observability(self, filters=None, epoch=None, observer=None):
        """Test the observability of the target, for the given epoch and observer

        Parameters
        ----------
        filters : vision.Filters
            The target must satisfy these filters to be considered observable


        epoch : vision.Epoch
            Date of observation

        altaz : astropy.coordinates.AltAz
            AltAz grid for the target

        observer : vision.Observer
            Observer location (only if altaz is not provided)
        """

        # ----------------------------------------
        # Simple argmin and argmax functions
        argmin = lambda vals: min(enumerate(vals), key=lambda nv: nv[1])[0]
        argmax = lambda vals: max(enumerate(vals), key=lambda nv: nv[1])[0]

        # ----------------------------------------
        # Inputs
        if filters is None:
            print("Target > is_observable > filters not provided")
            return False

        # Build AltAz grid from observer and epoch if not provided
        if self.altaz is None:
            if not isinstance(observer, vision.Observer):
                print("Target > is_observable > observer is not an Observer object")
                return False

            if not isinstance(epoch, vision.Epoch):
                print("Target > is_observable > epoch is not an Epoch object")
                return False

            # Convert observer/epoch into an AltAz grid
            self.compute_altaz(epoch, observer)

        # ----------------------------------------
        # Check Visibility from altitudes
        test_el_min = self.altaz.alt.value >= filters.altitude["min"]
        test_el_max = self.altaz.alt.value <= filters.altitude["max"]
        test_altitude = test_el_min & test_el_max

        # ----------------------------------------
        # Count duration of observability
        self.duration = (test_altitude.sum() - 1) * (5.0 / 60)
        if self.duration >= filters.duration_observability["min"]:
            self.is_visible = True

            # Highest-altitude epoch
            p_max = argmax(self.altaz.alt.value[test_altitude])
            self.peak = {
                "epoch": epoch.time_grid[test_altitude][p_max],
                "altitude": self.altaz.alt.value[test_altitude][p_max],
                "azimuth": self.altaz.az.value[test_altitude][p_max],
            }

            # First and last epoch of visibility
            p_min = argmin(epoch.time_grid[test_altitude].jd)
            self.start = {
                "epoch": epoch.time_grid[test_altitude][p_min],
                "altitude": self.altaz.alt.value[test_altitude][p_min],
                "azimuth": self.altaz.az.value[test_altitude][p_min],
            }

            p_max = argmax(epoch.time_grid[test_altitude].jd)
            self.end = {
                "epoch": epoch.time_grid[test_altitude][p_max],
                "altitude": self.altaz.alt.value[test_altitude][p_max],
                "azimuth": self.altaz.az.value[test_altitude][p_max],
            }

        else:
            self.is_visible = False
            self.duration = 0
            self.peak = {
                "epoch": None,
                "altitude": None,
                "azimuth": None,
            }
            self.start = {
                "epoch": None,
                "altitude": None,
                "azimuth": None,
            }
            self.end = {
                "epoch": None,
                "altitude": None,
                "azimuth": None,
            }

    # --------------------------------------------------------------------------------
    # Set plotting parametersCheck observability
    def set_markers(self, index_color=0, index_line=0):

        # Set from target type
        self.marker = vision.plot_style[self.type]["marker"]
        self.size = vision.plot_style[self.type]["size"]
        self.alpha = vision.plot_style[self.type]["alpha"]

        # Set from target index
        self.color = f"C{ index_color % 10 :d}"
        self.linestyle = vision.plot_params["linestyle"][
            index_line % len(vision.plot_params["linestyle"])
        ]

    # --------------------------------------------------------------------------------
    # Getters
    @property
    def type(self):
        return self._type

    @property
    def number(self):
        return self._number

    @property
    def name(self):
        return self._name

    @property
    def label(self):
        return self._label

    @property
    def coordinates(self):
        return self._coordinates

    # --------------------------------------------------------------------------------
    # Setters
    @type.setter
    def type(self, value):
        self._type = value

    @number.setter
    def number(self, value):
        self._number = value

    @name.setter
    def name(self, value):
        self._name = value

    @label.setter
    def label(self, value):
        self._label = value

    @coordinates.setter
    def coordinates(self, value):
        self._coordinates = value
