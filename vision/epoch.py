# Class for Epochs

import vision

from astropy.coordinates import AltAz
import astropy.units as u
from astropy.time import Time

# from numpy import linspace as np_linspace
# from numpy import cos as np_cos
import numpy as np


class ListOfEpochs:

    def __init__(self, epochs):
        # self.epochs = [Epoch(ep) for ep in epochs]
        self.epochs = epochs

    # --------------------------------------------------------------------------------
    # Getters
    @property
    def epochs(self):
        return [ep.date for ep in self._epochs]

    # Return a single epoch from its date
    def epoch(self, date):
        for ep in self._epochs:
            if ep.date == date:
                return ep

    # --------------------------------------------------------------------------------
    # Setters
    @epochs.setter
    def epochs(self, value):
        self._epochs = value


class Epoch:

    # --------------------------------------------------------------------------------
    def __init__(self, input=None, targets=None):
        """Create a new epoch object

        Parameters
        ----------
        date : str
            Calendar date of observation
        """
        self.date = None
        self.start = None
        self.end = None
        self.twilights = None
        self.time_grid = None
        self.altaz_grid = None

        self.Sun = None
        self.Moon = None
        self.targets = None

        if "targets" in locals():
            if isinstance(targets, vision.ListOfTargets):
                self.targets = targets.copy()

        if "input" in locals():
            if isinstance(input, str):
                self.date = input

            elif isinstance(input, dict):
                self.from_dict(input)

    # --------------------------------------------------------------------------------
    def copy(self):
        from copy import deepcopy

        return deepcopy(self)

    # --------------------------------------------------------------------------------
    # Retrieve epoch information from a dictionary (from ViSiON json)
    def from_dict(self, dic):
        """Create an Epoch object from a dictionary (ViSiON API)"""

        self.date = dic["date"]

        self.Sun = vision.Target(dic["Sun"])
        self.Sun.name = "Sun"
        self.Moon = vision.Target(dic["Moon"])
        self.Moon.name = "Moon"

        # TBD - what if not twilight!?
        self.twilight = {
            "civil": {
                "dusk": Time(dic["twilight"]["civil"]["dusk"], format="isot"),
                "dawn": Time(dic["twilight"]["civil"]["dawn"], format="isot"),
            },
            "nautical": {
                "dusk": Time(dic["twilight"]["nautical"]["dusk"], format="isot"),
                "dawn": Time(dic["twilight"]["nautical"]["dawn"], format="isot"),
            },
            "astronomical": {
                "dusk": Time(dic["twilight"]["astronomical"]["dusk"], format="isot"),
                "dawn": Time(dic["twilight"]["astronomical"]["dawn"], format="isot"),
            },
        }


    # --------------------------------------------------------------------------------
    # Export object to dictionary
    def to_dict(self):

        dic = {}
        dic["epoch"] = self.date
        dic["Sun"] = self.Sun.to_dict()
        dic["Moon"] = self.Moon.to_dict()
        
        target_list = []
        for t in self.targets.targets:
            target_list.append( self.targets.target(t).to_dict() )        
        dic["targets"] = target_list

        civil = {
            "dawn": self.twilight["civil"]["dawn"].isot,
            "dusk": self.twilight["civil"]["dusk"].isot
        }
        nautical = {
            "dawn": self.twilight["nautical"]["dawn"].isot,
            "dusk": self.twilight["nautical"]["dusk"].isot
        }
        astronomical = {
            "dawn": self.twilight["astronomical"]["dawn"].isot,
            "dusk": self.twilight["astronomical"]["dusk"].isot
        }
        dic["twilight"] = {
            "civil": civil,
            "nautical": nautical,
            "astronomical": astronomical
        }

        return dic

    # --------------------------------------------------------------------------------
    # Determine time range of the epoch
    def set_time_range(self, center="midnight", range="sun"):

        if center == "midnight":

            # Maximal time range
            ref_epoch = Time(self.Sun.transit["lower"]["time"])
            self.start = ref_epoch - 12 * u.hour
            self.end = ref_epoch + 12 * u.hour

            match range.lower():
                case "sun":
                    if isinstance(self.Sun.set, dict):
                        self.start = Time(self.Sun.set["time"])
                    if isinstance(self.Sun.rise, dict):
                        self.end = Time(self.Sun.rise["time"])

                case "civil":
                    if self.twilight["civil"]["dusk"] != None:
                        self.start = Time(self.twilight["civil"]["dusk"])
                    if self.twilight["civil"]["dawn"] != None:
                        self.end = Time(self.twilight["civil"]["dawn"])

                case "nautical":
                    if self.twilight["nautical"]["dusk"] != None:
                        self.start = Time(self.twilight["nautical"]["dusk"])
                    if self.twilight["nautical"]["dawn"] != None:
                        self.end = Time(self.twilight["nautical"]["dawn"])

                case "astronomical":
                    if self.twilight["astronomical"]["dusk"] != None:
                        self.start = Time(self.twilight["astronomical"]["dusk"])
                    if self.twilight["astronomical"]["dawn"] != None:
                        self.end = Time(self.twilight["astronomical"]["dawn"])

        elif center == "noon":

            # Maximal time range
            ref_epoch = Time(self.Sun.transit["upper"]["time"])
            self.start = ref_epoch - 12 * u.hour
            self.end = ref_epoch + 12 * u.hour

            match range.lower():
                case "sun":
                    if isinstance(self.Sun.rise, dict):
                        self.start = Time(self.Sun.rise["time"])
                    if isinstance(self.Sun.set, dict):
                        self.end = Time(self.Sun.set["time"])
                case "civil":
                    if self.twilight["civil"]["dusk"] != None:
                        self.start = Time(self.twilight["civil"]["dawn"])
                    if self.twilight["civil"]["dawn"] != None:
                        self.end = Time(self.twilight["civil"]["dusk"])

                case "nautical":
                    if self.twilight["nautical"]["dusk"] != None:
                        self.start = Time(self.twilight["nautical"]["dawn"])
                    if self.twilight["nautical"]["dawn"] != None:
                        self.end = Time(self.twilight["nautical"]["dusk"])

                case "astronomical":
                    if self.twilight["astronomical"]["dusk"] != None:
                        self.start = Time(self.twilight["astronomical"]["dawn"])
                    if self.twilight["astronomical"]["dawn"] != None:
                        self.end = Time(self.twilight["astronomical"]["dusk"])

        else:
            exit(1)

        # Add margins
        self.start = self.start - vision.plot_params["altitude"]["margin"]
        self.end = self.end + vision.plot_params["altitude"]["margin"]

    # --------------------------------------------------------------------------------
    # Build the time grid
    def set_time_grid(self, **kwargs):

        # Set Start/End date
        if (self.start == None) or (self.end == None):
            self.set_time_range(**kwargs)

        # Time grid, only between start and end
        step = 5
        nbd = int((36 + 24) * 60 / step + 1)
        self.time_grid = Time(self.date) + np.linspace(-24, 36, nbd) * u.hour
        valid = (self.time_grid >= self.start) & (self.time_grid <= self.end)
        self.time_grid = self.time_grid[valid]

    # --------------------------------------------------------------------------------
    # Build the AltAz grid
    def set_altaz_grid(self, observer, **kwargs):

        # Set time grid
        if self.time_grid == None:
            self.set_time_grid(**kwargs)

        # AltAz grid
        self.altaz_grid = AltAz(location=observer.location, obstime=self.time_grid)

    # --------------------------------------------------------------------------------
    # Time-Altitude plot
    def plot_altitude(self, observer=None, file=None, options=None):

        # ----------------------------------------
        # Set up graphics
        import matplotlib as mpl
        import matplotlib.pyplot as plt

        mpl.rcParams.update(vision.__figure_setup__)
        GOLDEN_RATIO = 1.6180
        TEXTWIDTH = 540.60236
        POINTS_TO_INCHES = 1 / 72.27
        figure_width = TEXTWIDTH * POINTS_TO_INCHES
        figure_height = figure_width / GOLDEN_RATIO

        fig, ax = plt.subplots(
            figsize=(figure_width, figure_height * 1.2),
            gridspec_kw={"top": 0.925, "bottom": 0.175, "left": 0.055, "right": 0.955},
        )

        bottom_line = [0, 0]
        top_line = [90, 90]

        # ----------------------------------------
        # Day/Night & Twilight regions
        colors = ["dimgray", "gray", "darkgray", "lightgray", "white"]
        if options.center == "midnight":
            events = [
                [self.start.datetime, self.end.datetime],
                [self.Sun.set["time"].datetime, self.Sun.rise["time"].datetime],
                [self.twilight["civil"]["dusk"].datetime, self.twilight["civil"]["dawn"].datetime],
                [
                    self.twilight["nautical"]["dusk"].datetime,
                    self.twilight["nautical"]["dawn"].datetime,
                ],
                [
                    self.twilight["astronomical"]["dusk"].datetime,
                    self.twilight["astronomical"]["dawn"].datetime,
                ],
            ]
        else:
            events = [
                [self.start.datetime, self.end.datetime],
                [
                    self.twilight["astronomical"]["dawn"].datetime,
                    self.twilight["astronomical"]["dusk"].datetime,
                ],
                [
                    self.twilight["nautical"]["dawn"].datetime,
                    self.twilight["nautical"]["dusk"].datetime,
                ],
                [self.twilight["civil"]["dawn"].datetime, self.twilight["civil"]["dusk"].datetime],
                [self.Sun.rise["time"].datetime, self.Sun.set["time"].datetime],
            ]

        for i, event in enumerate(events):
            ax.fill_between(
                event,
                bottom_line,
                top_line,
                facecolor=colors[i],
                rasterized=False,
                zorder=-100 + i,
            )

        # ----------------------------------------
        # Event Labels
        if self.Sun.set["time"].datetime >= self.start.datetime:
            ax.text(
                self.Sun.set["time"].datetime,
                vision.plot_params["altitude"]["events"]["name"],
                "Sunset",
                ha="center",
                va="bottom",
                color="black",
                fontsize="small",
            )
            ax.text(
                self.Sun.set["time"].datetime,
                vision.plot_params["altitude"]["events"]["time"],
                self.Sun.set["time"].iso[10:16],
                ha="center",
                va="bottom",
                color="black",
                fontsize="x-small",
            )

        if self.Sun.rise["time"].datetime <= self.end.datetime:
            ax.text(
                self.Sun.rise["time"].datetime,
                vision.plot_params["altitude"]["events"]["name"],
                "Sunrise",
                ha="center",
                va="bottom",
                color="black",
                fontsize="small",
            )
            ax.text(
                self.Sun.rise["time"].datetime,
                vision.plot_params["altitude"]["events"]["time"],
                self.Sun.rise["time"].iso[10:16],
                ha="center",
                va="bottom",
                color="black",
                fontsize="x-small",
            )

        if self.twilight["astronomical"]["dusk"].datetime >= self.start.datetime:
            ax.text(
                self.twilight["astronomical"]["dusk"].datetime,
                vision.plot_params["altitude"]["events"]["name"],
                "Twilight",
                ha="center",
                va="bottom",
                color="black",
                fontsize="small",
            )
            ax.text(
                self.twilight["astronomical"]["dusk"].datetime,
                vision.plot_params["altitude"]["events"]["time"],
                self.twilight["astronomical"]["dusk"].iso[10:16],
                ha="center",
                va="bottom",
                color="black",
                fontsize="x-small",
            )

        if self.twilight["astronomical"]["dawn"].datetime <= self.end.datetime:
            ax.text(
                self.twilight["astronomical"]["dawn"].datetime,
                vision.plot_params["altitude"]["events"]["name"],
                "Twilight",
                ha="center",
                va="bottom",
                color="black",
                fontsize="small",
            )
            ax.text(
                self.twilight["astronomical"]["dawn"].datetime,
                vision.plot_params["altitude"]["events"]["time"],
                self.twilight["astronomical"]["dawn"].iso[10:16],
                ha="center",
                va="bottom",
                color="black",
                fontsize="x-small",
            )

        # ----------------------------------------
        # Moon
        ax.plot(
            self.time_grid.datetime,
            self.Moon.altaz.alt.value,
            color=vision.plot_style["Moon"]["color"],
            alpha=vision.plot_style["Moon"]["alpha"],
            linestyle=vision.plot_style["Moon"]["line"],
            marker=vision.plot_style["Moon"]["marker"],
            ms=vision.plot_style["Moon"]["size"],
            markevery=vision.plot_style["Moon"]["markevery"],
            label="Moon",
        )

        # ----------------------------------------
        # Targets
        for kT, t in enumerate(self.targets.targets):
            if not self.targets.target(t).is_visible:
                continue

            # Full altitude curve
            before = self.time_grid.datetime < self.targets.target(t).start["epoch"]
            after = self.time_grid.datetime > self.targets.target(t).end["epoch"]
            for not_vis in [before, after]:
                ax.plot(
                    self.time_grid.datetime[not_vis],
                    self.targets.target(t).altaz.alt.value[not_vis],
                    color=self.targets.target(t).color,
                    alpha=vision.plot_params["alpha"],
                    linestyle=self.targets.target(t).linestyle,
                    marker=self.targets.target(t).marker,
                    ms=self.targets.target(t).size,
                    markevery=(kT, 12),
                )

            # Only visible portion
            vis = (self.time_grid.datetime >= self.targets.target(t).start["epoch"]) & (
                self.time_grid.datetime <= self.targets.target(t).end["epoch"]
            )

            ax.plot(
                self.time_grid.datetime[vis],
                self.targets.target(t).altaz.alt.value[vis],
                color=self.targets.target(t).color,
                alpha=self.targets.target(t).alpha,
                linestyle=self.targets.target(t).linestyle,
                marker=self.targets.target(t).marker,
                ms=self.targets.target(t).size,
                markevery=(kT, 12),
                label=self.targets.target(t).label,
            )

            # Label the curve
            ax.text(
                self.targets.target(t).peak["epoch"].datetime,
                self.targets.target(t).peak["altitude"] + 1,
                self.targets.target(t).label.strip(),
                ha="left",
                color=self.targets.target(t).color,
                alpha=self.targets.target(t).alpha,
            )

        # ----------------------------------------
        # Axes
        ax.set_xlim([self.start.datetime, self.end.datetime])
        ax.set_ylim([0, 90])
        ax.set_ylabel("Altitude (°)")
        ax.set_xlabel(f"Time (UTC, {self.date})")

        ax.xaxis.set_major_formatter(mpl.dates.DateFormatter("%H:%M"))

        # Y and airmass
        yticks = np.linspace(0, 90, 10)
        ax.set_yticks(yticks)
        airmass = 1 / np.cos(np.radians(90 - yticks))

        ax_right = ax.twinx()
        ax_right.set_yticks(yticks)
        am_ticks = [f"{am:.2f}" for am in airmass]
        am_ticks[0] = "airmass"
        ax_right.set_yticklabels(am_ticks, rotation=90, fontsize="small", va="center")
        ax_right.set_ylim([0, 90])

        # Legend
        ax.legend(loc="upper center", bbox_to_anchor=(0.5, -0.075), fancybox=True, ncol=6)

        # ----------------------------------------
        # Local sidereal time
        if not observer is None:

            xticks = ax.get_xticks()
            xlim = ax.get_xlim()

            lst = lst = [
                Time(mpl.dates.num2date(t))
                .sidereal_time("apparent", observer.location)
                .to_string(unit="hourangle")
                .split("m")[0]
                for t in xticks
            ]

            ax_top = ax.twiny()
            ax_top.set_xlim(xlim)
            ax_top.set_xticks(xticks)
            ax_top.set_xticklabels(lst, color="slategray", fontsize="small")
            ax.text(
                -0.055, 1.0075, "LST", color="slategray", fontsize="small", transform=ax.transAxes
            )
        # ----------------------------------------
        fig.savefig(file)
        plt.close()
        return fig, ax

    # --------------------------------------------------------------------------------
    # Sky plot
    def plot_sky(self, observer=None, file=None):

        # ----------------------------------------
        # Set up graphics
        import matplotlib as mpl
        import matplotlib.pyplot as plt

        mpl.rcParams.update(vision.__figure_setup__)
        GOLDEN_RATIO = 1.6180
        TEXTWIDTH = 540.60236
        POINTS_TO_INCHES = 1 / 72.27
        figure_width = TEXTWIDTH * POINTS_TO_INCHES
        figure_height = figure_width / GOLDEN_RATIO

        fig, ax = plt.subplots(
            figsize=(figure_width, figure_height),
            gridspec_kw={"top": 0.95, "bottom": 0.05, "left": 0.05, "right": 0.95},
            subplot_kw={"projection": "polar"},
        )

        # ----------------------------------------
        # Moon
        ax.plot(
            np.radians(self.Moon.altaz.az.value),
            self.Moon.altaz.alt.value,
            color=vision.plot_style["Moon"]["color"],
            alpha=vision.plot_style["Moon"]["alpha"],
            linestyle=vision.plot_style["Moon"]["line"],
            marker=vision.plot_style["Moon"]["marker"],
            ms=vision.plot_style["Moon"]["size"],
            markevery=vision.plot_style["Moon"]["markevery"],
            label="Moon",
        )

        # ----------------------------------------
        # Targets
        for kT, t in enumerate(self.targets.targets):
            if not self.targets.target(t).is_visible:
                continue

            # Full altitude curve
            before = self.targets.target(t).altaz.az.value < self.targets.target(t).start["azimuth"]
            after = self.targets.target(t).altaz.az.value > self.targets.target(t).end["azimuth"]
            for not_vis in [before, after]:
                ax.plot(
                    np.radians(self.targets.target(t).altaz.az.value[not_vis]),
                    self.targets.target(t).altaz.alt.value[not_vis],
                    color=self.targets.target(t).color,
                    alpha=vision.plot_params["alpha"],
                    linestyle=self.targets.target(t).linestyle,
                    marker=self.targets.target(t).marker,
                    ms=self.targets.target(t).size,
                    markevery=(kT, 12),
                )

            # Only visible portion
            vis = (
                self.targets.target(t).altaz.az.value >= self.targets.target(t).start["azimuth"]
            ) & (self.targets.target(t).altaz.az.value <= self.targets.target(t).end["azimuth"])
            ax.plot(
                np.radians(self.targets.target(t).altaz.az.value[vis]),
                self.targets.target(t).altaz.alt.value[vis],
                color=self.targets.target(t).color,
                alpha=self.targets.target(t).alpha,
                linestyle=self.targets.target(t).linestyle,
                marker=self.targets.target(t).marker,
                ms=self.targets.target(t).size,
                markevery=(kT, 12),
                label=self.targets.target(t).label,
            )

        # ----------------------------------------
        # Axes
        ax.set_rlim(90, 0)
        ax.set_theta_zero_location("N")
        ax.set_rticks([0, 30, 60, 90])
        ax.set_rlabel_position(-22.5)

        # Legend
        ax.legend(loc="upper center", bbox_to_anchor=(-0.25, 1.05), fancybox=True, ncol=1)

        # ----------------------------------------
        fig.savefig(file)
        plt.close()
        return fig, ax

    # --------------------------------------------------------------------------------
    # Getters
    @property
    def date(self):
        return self._date

    @property
    def target(self):
        return self._target

    # --------------------------------------------------------------------------------
    # Setters
    @date.setter
    def date(self, value):
        self._date = value
