# Class for Observer
import astropy.units as u
from astropy.coordinates import EarthLocation


class Observer:

    # --------------------------------------------------------------------------------
    def __init__(self, input):
        """Create a new Observer object

        Parameters
        ----------
        input: str | dict
            Name of the observer, or dictionary with observer information
        """
        self.timezone = None
        self.coordinates = None
        self.iaucode = None

        if isinstance(input, str):
            self.name = input

        elif isinstance(input, dict):
            self.name = input["name"]
            self.coordinates = input["coordinates"]
            # self.timezone = input["timezone"]

        if self.coordinates is None:
            print("code smth to resolve coordinates from name")

        # Location from coordinates
        coord_split = self.coordinates.split(":")
        if len(coord_split) == 2:
            if coord_split[0]=='geo':
                coords = coord_split[1].split(",")
                lat = float(coords[0])
                lon = float(coords[1])
                alt = float(coords[2])
                self.location = EarthLocation.from_geodetic(
                    lon=lon * u.deg, lat=lat * u.deg, height=alt * u.m
                )
            elif coord_split[0]=='iau':
                self.iaucode = coord_split[1]


    # --------------------------------------------------------------------------------
    # Export to dictionary (ViSiON API format)
    def to_dict(self):
        """Return a dictionary representation of the Vision.Observer object"""

        return {
            "name": self.name,
            "coordinates": self.coordinates,
        }

    # --------------------------------------------------------------------------------
    # Getters
    @property
    def name(self):
        return self._name

    # --------------------------------------------------------------------------------
    # Setters
    @name.setter
    def name(self, value):
        self._name = value
