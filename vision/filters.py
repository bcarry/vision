# Class for Filters

import vision


class Filters:

    # --------------------------------------------------------------------------------
    def __init__(self, input=None):
        """Create a new Filters object

        Parameters
        ----------
        input : dict
            A dictionary containing the filters (ViSiON API)
        """

        # General observability criteria
        self.altitude = vision.__default_filters__["altitude"]
        self.magnitude = vision.__default_filters__["magnitude"]
        self.solar_elongation = vision.__default_filters__["solar_elongation"]
        self.lunar_elongation = vision.__default_filters__["lunar_elongation"]
        self.galactic_latitude = vision.__default_filters__["galactic_latitude"]
        self.galactic_longitude = vision.__default_filters__["galactic_longitude"]
        self.duration_observability = vision.__default_filters__["duration_observability"]

        # Specific to Solar System
        self.phase_angle = vision.__default_filters__["phase_angle"]
        self.heliocentric_distance = vision.__default_filters__["heliocentric_distance"]
        self.range_to_observer = vision.__default_filters__["range_to_observer"]
        self.rate_of_motion = vision.__default_filters__["rate_of_motion"]
        self.angular_diameter = vision.__default_filters__["angular_diameter"]

        if "input" in locals():
            if isinstance(input, dict):
                self.from_dict(input)

    # --------------------------------------------------------------------------------
    def copy(self):
        from copy import deepcopy

        return deepcopy(self)

    # --------------------------------------------------------------------------------
    # Build object from a dictionary (from ViSiON json)
    def from_dict(self, dic):
        """Create an Filters object from a dictionary (ViSiON API)"""

        if "altitude" in dic.keys():
            if "min" in dic["altitude"].keys():
                self.altitude["min"] = dic["altitude"]["min"]
            if "max" in dic["altitude"].keys():
                self.altitude["max"] = dic["altitude"]["max"]

        if "magnitude" in dic.keys():
            if "min" in dic["magnitude"].keys():
                self.magnitude["min"] = dic["magnitude"]["min"]
            if "max" in dic["magnitude"].keys():
                self.magnitude["max"] = dic["magnitude"]["max"]

        if "solar_elongation" in dic.keys():
            if "min" in dic["solar_elongation"].keys():
                self.solar_elongation["min"] = dic["solar_elongation"]["min"]
            if "max" in dic["solar_elongation"].keys():
                self.solar_elongation["max"] = dic["solar_elongation"]["max"]

        if "lunar_elongation" in dic.keys():
            if "min" in dic["lunar_elongation"].keys():
                self.lunar_elongation["min"] = dic["lunar_elongation"]["min"]
            if "max" in dic["lunar_elongation"].keys():
                self.lunar_elongation["max"] = dic["lunar_elongation"]["max"]

        if "duration_observability" in dic.keys():
            if "min" in dic["duration_observability"].keys():
                self.duration_observability["min"] = dic["duration_observability"]["min"]
            if "max" in dic["duration_observability"].keys():
                self.duration_observability["max"] = dic["duration_observability"]["max"]

        if "galactic_latitude" in dic.keys():
            if "min" in dic["galactic_latitude"].keys():
                self.galactic_latitude["min"] = dic["galactic_latitude"]["min"]
            if "max" in dic["galactic_latitude"].keys():
                self.galactic_latitude["max"] = dic["galactic_latitude"]["max"]

        if "galactic_longitude" in dic.keys():
            if "min" in dic["galactic_longitude"].keys():
                self.galactic_longitude["min"] = dic["galactic_longitude"]["min"]
            if "max" in dic["galactic_longitude"].keys():
                self.galactic_longitude["max"] = dic["galactic_longitude"]["max"]

        # Specific to Solar System
        if "phase_angle" in dic.keys():
            if "min" in dic["phase_angle"].keys():
                self.phase_angle["min"] = dic["phase_angle"]["min"]
            if "max" in dic["phase_angle"].keys():
                self.phase_angle["max"] = dic["phase_angle"]["max"]

        if "heliocentric_distance" in dic.keys():
            if "min" in dic["heliocentric_distance"].keys():
                self.heliocentric_distance["min"] = dic["heliocentric_distance"]["min"]
            if "max" in dic["heliocentric_distance"].keys():
                self.heliocentric_distance["max"] = dic["heliocentric_distance"]["max"]

        if "range_to_observer" in dic.keys():
            if "min" in dic["range_to_observer"].keys():
                self.range_to_observer["min"] = dic["range_to_observer"]["min"]
            if "max" in dic["range_to_observer"].keys():
                self.range_to_observer["max"] = dic["range_to_observer"]["max"]


    # --------------------------------------------------------------------------------
    # Export to dictionary (ViSiON API format)
    def to_dict(self):
        """Return a dictionary representation of the Vision.Filters object"""

        return {
            "altitude": self.altitude,
            "magnitude": self.magnitude,
            "solar_elongation": self.solar_elongation,
            "lunar_elongation": self.lunar_elongation,
            "galactic_latitude": self.galactic_latitude,
            "galactic_longitude": self.galactic_longitude,
            "duration_observability": self.duration_observability,
            "phase_angle": self.phase_angle,
            "heliocentric_distance": self.heliocentric_distance,
            "range_to_observer": self.range_to_observer,
            "rate_of_motion": self.rate_of_motion,
            "angular_diameter": self.angular_diameter
        }
