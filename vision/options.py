# Class for Options

import vision


class Options:

    # --------------------------------------------------------------------------------
    def __init__(self, input=None):
        """Create a new Options object

        Parameters
        ----------
        input : dict
            A dictionary containing the options (ViSiON API)
        """

        # Initialize with default options
        self.from_dict(vision.__default_options__)

        if "input" in locals():
            if isinstance(input, dict):
                self.from_dict(input)

    # --------------------------------------------------------------------------------
    def copy(self):
        from copy import deepcopy

        return deepcopy(self)

    # --------------------------------------------------------------------------------
    # Build object from a dictionary (from ViSiON json)
    def from_dict(self, dic):
        """Create an Options object from a dictionary (ViSiON API)"""

        if "mime" in dic.keys():
            self.mime = dic["mime"]

        if "from" in dic.keys():
            self.user = dic["from"]

        if "mode" in dic.keys():
            self.center = dic["mode"]

        if "range" in dic.keys():
            self.range = dic["range"]

        if "max_output" in dic.keys():
            self.max_output = dic["max_output"]

        if "format_ra_dec" in dic.keys():
            self.format_ra_dec = dic["format_ra_dec"]

        if "sorting" in dic.keys():

            split = dic["sorting"].split(":")
            self.sort_by = split[0]

            if split[1] == "asc":
                self.ascending = True
            else:
                self.ascending = False

    # --------------------------------------------------------------------------------
    # Export to dictionary (ViSiON API format)
    def to_dict(self):
        """Return a dictionary representation of the Vision.Options object"""

        return {
            "mime": self.mime,
            "from": self.user,
            "mode": self.center,
            "range": self.range,
            "max_output": self.max_output,
            "format_ra_dec": self.format_ra_dec,
            "sorting": f"{self.sort_by}:{'asc' if self.ascending else 'desc'}",
        }
