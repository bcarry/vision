"""Visibility Service for Observing Nights"""

from .vision import Vision  # noqa
from .observer import Observer  # noqa
from .target import Target, ListOfTargets  # noqa
from .epoch import Epoch, ListOfEpochs  # noqa
from .options import Options  # noqa
from .filters import Filters  # noqa

from astropy.units import min as u_min

# Expose API to use


# Versioning
__version__ = "alpha"
__model_version__ = "4.4.1"

# Default options
__default_options__ = {
    "mime": "json",
    "user": None,
    "center": "midnight",
    "range": "Sun",
    "max_output": 10,
    "format_ra_dec": "decimal",
    "sort_by": "ra",
    "ascending": True,
}

# Default filters
__default_filters__ = {
    "altitude": {"min": 0.0, "max": 90.0},
    "magnitude": {"min": -99, "max": 99.0},
    "solar_elongation": {"min": 0.0, "max": 180.0},
    "lunar_elongation": {"min": 0.0, "max": 180.0},
    "galactic_latitude": {"min": -90.0, "max": 90.0},
    "galactic_longitude": {"min": 0.0, "max": 360.0},
    "duration_observability": {"min": 1.0},
    "phase_angle": {"min": 0.0, "max": 180.0},
    "heliocentric_distance": {"min": 0.0, "max": 9999.0},
    "range_to_observer": {"min": 0.0, "max": 9999.0},
    "rate_of_motion": {"min": 0.0, "max": 9999.0},
    "angular_diameter": {"min": 0.0, "max": 3600.0},
}

# Figure setup
__figure_setup__ = {
    # ----------
    # GENERAL SET-UP
    # ----------
    "backend": "pgf",  
    "backend_fallback": True,
    "toolbar": "toolbar2",
    "timezone": "UTC",  
    # ----------
    # LaTeX SET-UP
    # ----------
    "text.usetex": True,
    "pgf.rcfonts": False,
    # Packages required for figure compilation:
    "pgf.preamble": r"\usepackage{amsmath} "
    r"\usepackage[utf8x]{inputenc} "
    r"\usepackage[T1]{fontenc} "
    r"\usepackage{txfonts} "  # txfonts are used by A&A Journal
    r"\usepackage[default]{sourcesanspro} "
    r"\usepackage{pgfplots} "
    r"\usepgfplotslibrary{external} "
    r"\tikzexternalize "
    r"\usepackage{xcolor} ",
    # ----------
    # GENERAL
    # ----------
    "figure.figsize": (7.4803, 4.6232),
    "savefig.dpi": 300,
    "font.size": 10,
    "font.family": "serif",
    "text.color": "#000000",
    "axes.facecolor": "#ffffff", 
    "axes.edgecolor": "#000000", 
    "axes.linewidth": 0.5, 
    "axes.grid": True,  
    "grid.linestyle": "dotted", 
    "axes.titlesize": "large", 
    "axes.labelsize": "small", 
    "axes.labelcolor": "black",
    "axes.axisbelow": True,
    # ----------
    # Axe elements (line, text)
    # ----------
    "axes.xmargin": 0,
    "axes.ymargin": 0,
    "axes.spines.top": True,
    "axes.spines.right": True,
    "xtick.major.size": 4,
    "xtick.minor.size": 2,
    "xtick.major.pad": 2, 
    "xtick.major.width": 0.5,
    "xtick.minor.width": 0.3,
    "xtick.minor.visible": True,
    "xtick.minor.pad": 2, 
    "xtick.color": "black",
    "xtick.top": True,
    "xtick.labelsize": "8",
    "xtick.direction": "in",
    "ytick.major.size": 4,
    "ytick.major.width": 0.5,
    "ytick.minor.width": 0.2,
    "ytick.minor.size": 2,
    "ytick.major.pad": 2, 
    "ytick.minor.pad": 2,  
    "ytick.major.width": 0.5,
    "ytick.minor.width": 0.3,
    "ytick.right": True,
    "ytick.color": "black",  
    "ytick.labelsize": "8",  
    "ytick.direction": "in", 
    "ytick.minor.visible": True,
    "grid.color": "black",  
    "grid.linestyle": ":",  
    "grid.linewidth": "0.2",  
    "legend.fontsize": "small",
    "legend.fancybox": False, 
    "lines.linewidth": 1.0,  
    "lines.antialiased": True, 
}

# Lines and symbols
plot_style = {
    "Moon": {
        "color": "black",
        "marker": "o",
        "line": (0,(5,7)),
        "size": 5,
        "alpha": 0.6,
        "markevery": 12,
    },
    "coordinates": {
        "marker": "*",
        "size": 5,
        "alpha": 0.8,
    },
    "simbad": {
        "marker": "*",
        "size": 5,
        "alpha": 0.8,
    },
    "asteroid": {
        "marker": "o",
        "size": 4,
        "alpha": 0.8,
    },
    "comet": {
        "marker": "s",
        "size": 4,
        "alpha": 0.8,
    },
    "planet": {
        "marker": "P",
        "size": 4,
        "alpha": 0.8,
    },
    "satellite": {
        "marker": "X",
        "size": 5,
        "alpha": 0.8,
    },
}

plot_params = {
    "altitude": {
        "events": {
            "name": 96.5,
            "time": 94.5
            },
        "margin": 30 * u_min,
        "y_top_labels": 96.5, 
        "y_hour_labels": 94.5
    },
    "linestyle": ["-", "--", "-.", ":", (0, (4, 2, 1, 2, 1, 2))],
    "alpha": 0.4,
}