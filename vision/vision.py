# Vision main class

import os
import vision
import json

import time
from numpy import linspace as np_linspace
# from numpy.random import default_rng


class Vision:

    # --------------------------------------------------------------------------------
    def __init__(self, file, targets=None, epochs=None, observer=None, filters=None, options=None):
        """Create a new Vision object

        Parameters
        ----------
        file : str
            Name of a json file containing the result from Vision API

        targets : list
            List of targets

        epochs : list
            List of epochs

        observer : vision.Observer
            Observer object

        filters : vision.Filter
            Observability constrains
        """

        self.version = None
        self.observer = observer
        self.epochs = epochs
        self.targets = targets
        self.filters = filters
        self.options = options
        self.dict = None

        if "file" in locals():
            if isinstance(file, str):
                self.from_json(file)

    # --------------------------------------------------------------------------------
    def copy(self):
        from copy import deepcopy

        return deepcopy(self)

    # --------------------------------------------------------------------------------
    def from_json(self, file):

        if not os.path.isfile(file):
            raise FileNotFoundError(f"File {file} not found")

        # ----------------------------------------
        # Read json file
        with open(file, "r") as f:
            self.dict = json.load(f)

        # ----------------------------------------
        # Build version tag
        self.version = self.dict["vision"]
        if self.version["version"] != vision.__model_version__:
            print(
                f"Warning: Attempting to use a json file which version {self.version} differs from current version {vision.__model_version__}"
            )

        # ----------------------------------------
        # Build observer
        self.observer = vision.Observer(self.dict["request"]["location"])

        # ----------------------------------------
        # Build observability filters
        self.filters = vision.Filters(self.dict["request"]["filters"])

        # ----------------------------------------
        # Build options
        self.options = vision.Options(self.dict["request"]["options"])

        # ----------------------------------------
        # Build the list of Targets
        targets = []
        for target_type in self.dict["request"]["targets"].keys():

            # print(f"\n {target_type}")
            match target_type:
                case "asteroid":
                    for t in self.dict["request"]["targets"][target_type]:
                        targets.append(vision.Target(asteroid=t))

                case "planet":
                    for t in self.dict["request"]["targets"][target_type]:
                        targets.append(vision.Target(planet=t))

                case "comet":
                    for t in self.dict["request"]["targets"][target_type]:
                        targets.append(vision.Target(comet=t))

                case "satellite":
                    for t in self.dict["request"]["targets"][target_type]:
                        targets.append(vision.Target(satellite=t))

                case "coordinates":
                    for t in self.dict["request"]["targets"][target_type]:
                        targets.append(vision.Target(coordinates=t))

                case "simbad":
                    for t in self.dict["request"]["targets"][target_type]:
                        targets.append(vision.Target(cds=t, resolve=False))

        # Set up markers and lines
        # rng = default_rng()
        # rints = rng.integers(low=0, high=9, size=len(targets))
        for i, t in enumerate(targets):
            t.set_markers(index_color=i, index_line=i)

        self.targets = vision.ListOfTargets(targets)

        # ----------------------------------------
        # Build the list of epochs
        epochs = []
        for ep_data in self.dict["response"]["data"]:

            # Create a new Epoch object
            obs = vision.Epoch(ep_data)

            # Build time and altaz grids
            obs.set_time_range(center=self.options.center, range=self.options.range)
            obs.set_time_grid()
            obs.set_altaz_grid(self.observer)

            # Compute the coordinates of the Moon
            obs.Moon.compute_altaz(obs, self.observer)

            # Update targets and compute observability
            obs.targets = self.targets.copy()
            for input_target in ep_data["targets"]:

                # TBD Need smth robust to track targets
                if input_target["id"] in obs.targets.identifiers:

                    # Update properties for the current date
                    obs.targets.target(input_target["id"]).update_from_dict(input_target)

                    # Check observability
                    obs.targets.target(input_target["id"]).is_observable(filters=self.filters)

                    if not obs.targets.target(input_target["id"]).is_visible:
                        continue

                    # Compute targets AltAz coordinates
                    obs.targets.target(input_target["id"]).compute_altaz(
                        epoch=obs,
                        observer=self.observer,
                    )

                    # Compute observability range
                    obs.targets.target(input_target["id"]).compute_observability(
                        filters=self.filters, epoch=obs
                    )

            epochs.append(obs)

        self.epochs = vision.ListOfEpochs(epochs)

    # --------------------------------------------------------------------------------
    def check_observability(self):
        """Check the observability of all targets, for all epochs"""
        # Loop over epoch
        for e in self.epochs.epochs:

            # Update time and altaz grids
            self.epochs.epoch(e).set_time_range(
                center=self.options.center, range=self.options.range
            )
            self.epochs.epoch(e).set_time_grid()
            self.epochs.epoch(e).set_altaz_grid(observer=self.observer)

            # Update the Moon
            self.epochs.epoch(e).Moon.compute_altaz(
                epoch=self.epochs.epoch(e),
                observer=self.observer,
            )

            # Loop over targets
            for t in self.epochs.epoch(e).targets.targets:

                # Check observability
                self.epochs.epoch(e).targets.target(t).is_observable(filters=self.filters)

                if not self.epochs.epoch(e).targets.target(t).is_visible:
                    continue

                # Compute targets AltAz coordinates
                self.epochs.epoch(e).targets.target(t).compute_altaz(
                    epoch=self.epochs.epoch(e),
                    observer=self.observer,
                )

                # Compute observability range
                self.epochs.epoch(e).targets.target(t).compute_observability(
                    filters=self.filters, epoch=self.epochs.epoch(e)
                )

    # --------------------------------------------------------------------------------
    def to_latex(self, path=None):
        """Export the Vision object to a latex file

        Parameters
        ----------
        path : str
            Path to the directory where the latex files will be saved

        """
        if path is None:
            print("You must provide a path")
            exit(0)

        if not os.path.exists(path):
            print("Path does not exist")
            exit(0)

        os.system("cp " + os.path.join("..", "latex", "main.tex ") + os.path.join(path))
        os.system("cp " + os.path.join("..", "latex", "logo-vision.pdf ") + os.path.join(path))

        # Create figures
        for i, e in enumerate(self.epochs.epochs):
            fig, ax = self.epochs.epoch(e).plot_altitude(
                observer=self.observer,
                file=os.path.join(path, f"alt_{e}.pgf"),
                options=self.options,
            )
            fig, ax = self.epochs.epoch(e).plot_sky(
                observer=self.observer,
                file=os.path.join(path, f"sky_{e}.pgf"),
            )

        # Table of Content with epochs
        with open(os.path.join(path, "list.tex"), "w") as f:
            f.write("\\begin{itemize}\n")
            for i, e in enumerate(self.epochs.epochs):
                f.write(
                    r"  \item[$\bullet$] \hyperlink{"
                    + f"ep:{i+1}"
                    + r"}{\textsc{Chart "
                    + f"{i+1}"
                    + "}: "
                    + f"{e}"
                    + r"}"
                    + "\n"
                )
            f.write("\\end{itemize}\n")

        # For each epoch, import figure and write table
        with open(os.path.join(path, "epochs.tex"), "w") as f:
            for i, e in enumerate(self.epochs.epochs):

                # Newpage and counter
                f.write("\\clearpage\n\n")
                f.write("\\renewcommand{\\epoch}{" + f"{e}" + "}\n")
                f.write("~\n")

                # Import figures
                f.write(r"\hypertarget{" + f"ep:{i+1}" + "}{\\begin{figure}[!ht]\n")
                f.write("  \\begin{center}\n")
                f.write(r"    \input{" + f"alt_{e}.pgf" + "}\n")
                f.write(r"    \input{" + f"sky_{e}.pgf" + "}\n")
                f.write("  \\end{center}\n")
                f.write("\\end{figure}}\n\n")

                # Summary table
                f.write("\\clearpage\n")
                f.write("~\n")
                f.write("\\vspace{1em}\n")

                f.write("\\begin{table}[h]\n")
                f.write("  \\begin{tabular}{l|r|rr|rr|rrrrrr|rr|rrrr}\n")
                f.write(
                    " &  & \\multicolumn{2}{c}{Eq.} "
                    + " & \\multicolumn{2}{c}{Gal.} "
                    + " & \\multicolumn{2}{c}{Start} "
                    + " & \\multicolumn{2}{c}{Peak} "
                    + " & \\multicolumn{2}{c}{End} "
                    + " & \\multicolumn{2}{c}{Elongation} "
                    + " & \\multicolumn{4}{c}{Solar System} "
                    + "\\\\ \n"
                )
                f.write(
                    "Target & V "
                    + "& RA & Dec "
                    + "& Long. & Lat. "
                    + "& Alt. & Az "
                    + "& Alt. & Az  "
                    + "& Alt. & Az "
                    + "& $\\widehat{\\rm SEO}$ & $\\widehat{\\rm MEO}$ "
                    + "& $r$ & $\\Delta$ & $\\alpha$ & Rate "
                    + "\\\\ \n"
                )
                f.write("\\hline \n")
                # f.write("\\vfill\n")

                for t in self.epochs.epoch(e).targets.targets:
                    loc = self.epochs.epoch(e).targets.target(t)

                    if not loc.mag is None:
                        mag = f"{loc.mag:.1f}"
                    else:
                        mag = ""

                    if not loc.start["altitude"] is None:
                        start_alt = f"{loc.start['altitude']:.0f}"
                        start_az = f"{loc.start['azimuth']:.0f}"
                    else:
                        start_alt = ""
                        start_az = ""

                    if not loc.peak["altitude"] is None:
                        peak_alt = f"{loc.peak['altitude']:.0f}"
                        peak_az = f"{loc.peak['azimuth']:.0f}"
                    else:
                        peak_alt = ""
                        peak_az = ""

                    if not loc.end["altitude"] is None:
                        end_alt = f"{loc.end['altitude']:.0f}"
                        end_az = f"{loc.end['azimuth']:.0f}"
                    else:
                        end_alt = ""
                        end_az = ""

                    if loc.type in ["planet", "asteroid", "comet", "satellite"]:
                        geo_dist = f"{loc.range:.1f}"
                        helio_dist = f"{loc.heliodist:.1f}"
                        phase = f"{loc.phase:.0f}"
                        rate = f"{loc.rate:.0f}"
                    else:
                        geo_dist = ""
                        helio_dist = ""
                        phase = ""
                        rate = ""

                    f.write(
                        f"{loc.label} & {mag} "
                        # RA/Dec
                        + f" & {loc.coordinates.ra.deg:.0f}"
                        + f" & {loc.coordinates.dec.deg:.0f}"
                        # Galactic longitude/lattiude
                        + f" & {loc.coordinates.galactic.l.deg:.0f}"
                        + f" & {loc.coordinates.galactic.b.deg:.0f}"
                        # Start alt-az
                        + f" & {start_alt}"
                        + f" & {start_az}"
                        # Peak alt-az
                        + f" & {peak_alt}"
                        + f" & {peak_az}"
                        # End alt-az
                        + f" & {end_alt}"
                        + f" & {end_az}"
                        # Elongations
                        + f" & {loc.solar_elongation:.0f}"
                        + f" & {loc.lunar_elongation:.0f}"
                        # Solar system specifics
                        + f" & {geo_dist}"
                        + f" & {helio_dist}"
                        + f" & {phase}"
                        + f" & {rate}"
                        + r"\\"
                        + " \n"
                    )

                f.write("\\hline \n")
                f.write("  \\end{tabular}\n")
                f.write("\\end{table}\n")

    # --------------------------------------------------------------------------------
    def compile_latex(self, path=None):
        """Compile the latex files to a pdf

        Parameters
        ----------
        path : str
            Path to the directory where the latex files are saved
        """

        if path is None:
            print("You must provide a path")
            exit(0)

        # Add some test on files in the directory
        os.system("cd " + path + " && xelatex main.tex")

    # --------------------------------------------------------------------------------
    def to_pdf(self, path=None):
        """Export the figure and summary in latex and compile to a pdf

        Parameters
        ----------
        path : str
            Path to the directory where the latex files are saved
        """

        if path is None:
            print("You must provide a path")
            exit(0)

        self.to_latex(path)
        self.compile_latex(path)

    # --------------------------------------------------------------------------------
    def to_dict(self):
        """Return a dictionary representation of the Vision object"""

        from astropy.time import Time

        # Load ViSiON template
        file = os.path.join(vision.__path__[0], "..", "data", "ephevision_template.json")
        with open(file, "r") as f:
            dic = json.load(f)

        # Fill the request
        dic["link"]["self"] = ""
        dic["vision"]["datetime"] = Time.now().isot
        dic["request"]["location"] = self.observer.to_dict()
        dic["request"]["filters"] = self.filters.to_dict()
        dic["request"]["options"] = self.options.to_dict()

        # Targets
        type_arr = []
        name_arr = []
        for t in self.targets.targets:
            type_arr.append(self.targets.target(t).type)
            name_arr.append(self.targets.target(t).name)

        targets = {}
        for t in ["asteroid", "comet", "planet", "satellite", "coordinates", "simbad"]:
            target_list = []
            for i in [i for i, x in enumerate(type_arr) if x == t]:
                target_list.append(name_arr[i])
            targets[t] = target_list

        dic["request"]["targets"] = targets

        # Epochs
        dic["request"]["epochs"] = {
            "start": self.epochs.epochs[0],
            "step": int(Time(self.epochs.epochs[1]).jd - Time(self.epochs.epochs[0]).jd),
            "ndates": len(self.epochs.epochs),
        }

        # Response data
        epoch_list = []
        for ie, e in enumerate(self.epochs.epochs):
            epoch_list.append( self.epochs.epoch(e).to_dict() )
        dic["response"]["data"] = epoch_list

        self.dict = dic
        return dic

    # --------------------------------------------------------------------------------
    def update_dict(self):
        """Update the dictionary representation of the Vision object"""

        # Filters
        self.dict["request"]["filters"] = self.filters.to_dict()

        # Options
        self.dict["request"]["options"] = self.options.to_dict()

        # Epochs/Targets (in response)
        epochs_dict = [d["date"] for d in self.dict["response"]["data"]]
        for e in self.epochs.epochs:
            p_e = epochs_dict.index(e)
            targets_dict = [t["id"] for t in self.dict["response"]["data"][p_e]["targets"]]
            for t in self.epochs.epoch(e).targets.targets:
                p_t = targets_dict.index(t)

                # Visibility
                self.dict["response"]["data"][p_e]["targets"][p_t]["is_visible"] = (
                    self.epochs.epoch(e).targets.target(t).is_visible
                )
                self.dict["response"]["data"][p_e]["targets"][p_t]["duration_observability"] = (
                    self.epochs.epoch(e).targets.target(t).duration
                )

                # Start-Peak-End epochs and coordinates
                if self.epochs.epoch(e).targets.target(t).is_visible:
                    self.dict["response"]["data"][p_e]["targets"][p_t]["start"] = {
                        "epoch": self.epochs.epoch(e).targets.target(t).start["epoch"].iso,
                        "altitude": self.epochs.epoch(e).targets.target(t).start["altitude"],
                        "azimuth": self.epochs.epoch(e).targets.target(t).start["azimuth"],
                    }

                    self.dict["response"]["data"][p_e]["targets"][p_t]["peak"] = {
                        "epoch": self.epochs.epoch(e).targets.target(t).peak["epoch"].iso,
                        "altitude": self.epochs.epoch(e).targets.target(t).peak["altitude"],
                        "azimuth": self.epochs.epoch(e).targets.target(t).peak["azimuth"],
                    }

                    self.dict["response"]["data"][p_e]["targets"][p_t]["end"] = {
                        "epoch": self.epochs.epoch(e).targets.target(t).end["epoch"].iso,
                        "altitude": self.epochs.epoch(e).targets.target(t).end["altitude"],
                        "azimuth": self.epochs.epoch(e).targets.target(t).end["azimuth"],
                    }
                else:
                    self.dict["response"]["data"][p_e]["targets"][p_t]["start"] = {
                        "epoch": None,
                        "altitude": None,
                        "azimuth": None,
                    }

                    self.dict["response"]["data"][p_e]["targets"][p_t]["peak"] = {
                        "epoch": None,
                        "altitude": None,
                        "azimuth": None,
                    }

                    self.dict["response"]["data"][p_e]["targets"][p_t]["end"] = {
                        "epoch": None,
                        "altitude": None,
                        "azimuth": None,
                    }

    # --------------------------------------------------------------------------------
    def to_json(self, file):
        """Export the Vision object to a json file

        Parameters
        ----------
        file : str
            Name of the json file
        """

        self.to_dict()
        with open(file, "w", encoding="utf-8") as f:
            json.dump(self.dict, f, indent=4)
