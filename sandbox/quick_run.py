import sys

sys.path.append("..")
import time


t0 = time.time()
import vision

t1 = time.time()
print(f"import vision: {t1 - t0:.3f}s")


file = "../data/ephevision-Caussols-midnight.json"

t0 = time.time()
view = vision.Vision(file)
t1 = time.time()
print(f"Load file:{t1 - t0:.3f}s\n")

t0 = time.time()
test = view.to_dict()
t1 = time.time()
print(f"Build dict:{t1 - t0:.3f}s\n")

t0 = time.time()
view.to_json("test.json")
t1 = time.time()
print(f"Write file:{t1 - t0:.3f}s\n")

# import json
# with open("test.json", "w") as outfile: 
#     json.dump(test, outfile)