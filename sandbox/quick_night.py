import os
import sys

sys.path.append("..")
import time

t0 = time.time()
import vision

t1 = time.time()
print(f"Import vision: {t1 - t0:.3f}s")


# Read input file
file = "../data/ephevision-Caussols-noon.json"
file = "../data/ephevision-Caussols-midnight.json"

t0 = time.time()
view = vision.Vision(file)
t1 = time.time()
print(f"Read input file: {t1 - t0:.3f}s")



# Change options
change = True
if change:
    view.options.range = "sun"
    view.filters.altitude["min"] = 15

    t0 = time.time()
    view.check_observability()
    t1 = time.time()
    print(f"Check observability: {t1 - t0:.3f}s")


path = "test"
if not os.path.exists(path):
    os.mkdir(path)

view.to_latex(path)
view.compile_latex(path)

# Equivalent to:
# view.to_pdf(path)