import sys

sys.path.append("..")
import time

t0 = time.time()
import vision
t1 = time.time()
print(f"Import vision: {t1 - t0:.3f}s")


# Read input file
file = "../data/ephevision-Caussols-noon.json"

t0 = time.time()
view = vision.Vision(file)
t1 = time.time()
print(f"Read input file: {t1 - t0:.3f}s")


# Change options
view.options.range = "sun"
view.filters.altitude["min"] = 15

t0 = time.time()
view.check_observability()
t1 = time.time()
print(f"Check observability: {t1 - t0:.3f}s")


# Create figures
t0 = time.time()
for e in view.epochs.epochs:

    print(e, view.epochs.epoch(e).start, view.epochs.epoch(e).Sun.rise['time'])

    fig, ax =  view.epochs.epoch(e).plot_altitude(observer=view.observer, file=f'../latex/alt_day_{e}.png', options=view.options)
    # fig, ax =  view.epochs.epoch(e).plot_sky(observer=view.observer, file=f'../latex/sky_day_{e}.png')
    
    # fig, ax =  view.epochs.epoch(e).plot_altitude(observer=view.observer, file=f'../latex/alt_day_{e}.pgf', options=view.options)
    # fig, ax =  view.epochs.epoch(e).plot_sky(observer=view.observer, file=f'../latex/sky_day_{e}.pgf')

t1 = time.time()
print(f"Create figures: {t1 - t0:.3f}s\n")