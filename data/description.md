---
title: "Description de ViSiON"
author: [B. Carry]
date: "2024-04-02"
...
# Situation actuelle
- Le service s'appelle ViSiON, voir 
  [Carry & Berthier 2018](https://ui.adsabs.harvard.edu/abs/2018P&SS..164...79C/abstract)

- Son API est décrite ici: [https://ssp.imcce.fr/webservices/miriade/api/vision/](https://ssp.imcce.fr/webservices/miriade/api/vision/)

- Pas de formulaire récent, mais celui de nos anciennes pages: 
  [http://vo.imcce.fr/webservices/miriade/?forms](http://vo.imcce.fr/webservices/miriade/?forms)

- Code IDL, bash, etc (je peux fournir les sources IDL, mais pas très utile IMHO)

- Solution alternative qui est apparue récemment: 
  [https://airmass.org/](https://airmass.org/)

  - Mais pas scriptable
  - Mais une seule date à la fois
 
# But haut niveau

Mettre en place une librairie python pour assurer:

- Côté serveur:
  1. Lectures des résultats d'éphémérides (format `json`)
  2. Sélection des cibles suivant critères (filtrage)
  3. Production de graphiques et tables
  4. Wrap up dans un PDF (optionnel, via compilation latex? Avantage: figure en `pgf`)


- Côté client:
  1. Requêtes des éphémérides au serveur via API
  2. Lectures des résultats d'éphémérides (possibilité de save à considérer) (format `json`)
  3. Sélection des cibles suivant critères (filtrage)
  4. Production de graphiques et tables
  5. Wrap up dans un PDF (optionnel, via compilation latex?)


# Etapes du traitement

0. Instantiation / Configuration de base 
   - `url` de l'API
   - Filtres par défaut
   - Choix graphiques

1. Lecture du fichier
   - Contient les critères de sélection
   - Contient les paramètres (éphémérides) pour les objets, pour chaque date demandée
   - Voir le fichier `metadata.json` pour une description des champs

2. Pour chaque date
   - Appliquer les filtres sur les objets
   - Combien d'objets sont observables = N_obs
   - Si N_obs > 0 alors produire des graphiques / tables pour la date

3. A la fin
   - Regrouper les informations sur N_obs vs date
   - Construire la sortie avec toutes les nuits valident (tables+gfx)
     - VOTable
     - PDF (via latex sauf si meilleure idée)


# Liste au Père Noël
- CLI qui permet de créer les requêtes en ligne de commande
- Lib python pour côté serveur et client
- Si client, après étape 1, pouvoir changer les filtres pour appliquer étape 2 pleins de fois
- Librairie mise à dispo de la communauté (PyPI...)
- Documentation (readthedocs.io)

# TODO 02/04/2024
- [ ] BCa - Estimer le gain/perte en temps entre astropy ou Eproc pour 1 vs N pas par date
- [ ] JPG - Licence?
- [ ] JPG - Où placer les sources?
  - github
  - renater sourcesup
  - ?
