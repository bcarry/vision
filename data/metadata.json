{
   "vision": {
      "version": "<version>",
      "datetime": "<datetime>"
   },

   "link": {
      "self": "<link>"
   },

   "request": {

      "targets": { 
         "asteroid": [ "id_target_1", "id_target_2", "id_target_n" ],
         "comet": ["id_target_1"],
         "satellite": ["id_target_1"],
         "planet": ["id_target_1"],
         "simbad": ["id_target_1"],
         "coordinates": ["RA_1,DEC_1","RA_2,DEC_2"]
      },

      "location": {
         "coordinates": {
            "type": "text",
            "label": "Coordinates of the observer",
            "description": "Observer coordinates formatted as 'geo' URI (RFC 5870)",
            "format": "<geo|iau>:<lat,long[,alt]>|<IAU code>",
            "unit": "deg,deg,m|none"
         },
         "name": {
            "type": "text",
            "label": "Location name",
            "description": "Name of the observing place",
            "ucd": "meta.id"
         }
      },

      "epochs": {
         "list": [ "date_1", "date_2" ],
         "start": {
            "type": "text",
            "label": "First epoch of computation",
            "description": "First epoch of computation",
            "unit": "YYYY-MM-DD"
         },
         "end": {
            "type": "text",
            "label": "Last epoch of computation",
            "description": "Last epoch of computation",
            "unit": "YYYY-MM-DD"
         },
         "step": {
            "type": "int",
            "label": "Interval between two epochs",
            "description": "Time interval between two epochs of computation",
            "unit": "day"
         },
         "ndates": {
            "type": "int",
            "label": "Number of epochs of computation",
            "description": "Number of epochs of computation"
         }
      },

      "filters": {
         "elevation": {
            "type": "float",
            "label": "Range of elevation",
            "min": "<min_elevation>", 
            "max": "<max_elevation>", 
            "description": "Range of elevation for selection",
            "unit": "deg",
            "symbol": "H"
         },
         "magnitude": {
            "type": "float",
            "label": "Apparent magnitude",
            "min": "<min_magnitude>", 
            "max": "<max_magnitude>", 
            "description": "Range of apparent magnitude for selection",
            "unit": "mag"
         },
         "solar_elongation": {
            "type": "float",
            "label": "Solar elongation",
            "min": "<min_solar_elongation>", 
            "max": "<max_solar_elongation>", 
            "description": "Range of Solar elongation for selection",
            "unit": "deg",
            "symbol": "Ψ_☉"
         },
         "lunar_elongation": {
            "type": "float",
            "label": "Lunar elongation",
            "min": "<min_lunar_elongation>", 
            "max": "<max_lunar_elongation>", 
            "description": "Range of Lunar elongation for selection",
            "unit": "deg",
            "symbol": "Ψ_☾"
         },
         "phase_angle": {
            "type": "float",
            "label": "Phase angle",
            "min": "<min_phase_angle>", 
            "max": "<max_phase_angle>", 
            "description": "Range of phase angle for selection",
            "unit": "deg",
            "symbol": "𝛾"
         },
         "galactic_latitude": {
            "type": "float",
            "label": "Galactic latitude",
            "min": "<min_galactic_latitude>", 
            "max": "<max_galactic_latitude>", 
            "description": "Range of galactic latitude for selection",
            "unit": "deg",
            "symbol": "β_G"
         },
         "heliocentric_distance": {
            "type": "float",
            "label": "Heliocentric distance",
            "min": "<min_heliocentric_distance>", 
            "max": "<max_heliocentric_distance>", 
            "description": "Range of heliocentric distance for selection",
            "unit": "au",
            "symbol": "𝚫"
         },
         "range_to_observer": {
            "type": "float",
            "label": "Range to observer",
            "min": "<min_range_to_observer>", 
            "max": "<max_range_to_observer>", 
            "description": "Range of range to observer for selection",
            "unit": "au",
            "symbol": "r"
         },
         "angular_diameter": {
            "type": "float",
            "label": "Apparent angular diameter",
            "min": "<min_angular_diameter>", 
            "max": "<max_angular_diameter>", 
            "description": "Range of angular diameter for selection",
            "unit": "arcsec"
         },
         "duration_observability": {
            "type": "float",
            "label": "Length of observability window",
            "min": "<min_observability>", 
            "max": "<max_observability>", 
            "description": "Duration of the observability window",
            "unit": "min"
         }
      },

      "options": {
         "mime": "json",
         "from": "<user>",
         "dark": "<sun|civil|nautical|astronomical|24h>",
         "max_output": "10",
         "format_ra_dec": 0,
         "sorting": "ra:asc"
      }
   }, 

   "response": {

      "data": [
         {
            "date": "YYYY-MM-DD",
            "sun": { 
               "rise": "float", 
               "az_rise": "float",
               "transit": "float", 
               "elevation_transit": "float", 
               "set": "float", 
               "az_set": "float",
               "twilight": {
                  "civil": "float",
                  "nautical": "float",
                  "astronomical": "float"
               },
               "duration_observability": "float", 
               "magnitude": "float",
               "RA": "float",
               "DEC": "float",
               "galatic_longitude": "float",
               "galatic_latitude": "float",
               "rate_of_motion": "float",
               "angular_diameter": "float",
               "obs_distance": "float",
               "lunar_elongation": "float"
            },
            "moon": { 
               "rise": "float", 
               "az_rise": "float",
               "transit": "float", 
               "elevation_transit": "float", 
               "set": "float", 
               "az_set": "float",
               "duration_observability": "float", 
               "magnitude": "float",
               "RA": "float",
               "DEC": "float",
               "galatic_longitude": "float",
               "galatic_latitude": "float",
               "rate_of_motion": "float",
               "angular_diameter": "float",
               "helio_distance": "float",
               "obs_distance": "float",
               "phase_angle": "float",
               "solar_elongation": "float"
            },
            "sso": [
               { 
                  "name": "string",
                  "rise": "float", 
                  "az_rise": "float",
                  "transit": "float", 
                  "elevation_transit": "float", 
                  "set": "float", 
                  "az_set": "float",
                  "duration_observability": "float", 
                  "magnitude": "float",
                  "RA": "float",
                  "DEC": "float",
                  "galatic_longitude": "float",
                  "galatic_latitude": "float",
                  "rate_of_motion": "float",
                  "angular_diameter": "float",
                  "helio_distance": "float",
                  "obs_distance": "float",
                  "phase_angle": "float",
                  "solar_elongation": "float",
                  "lunar_elongation": "float"
               }
            ] 
         }
      ],

      "description": {
         "date": "Date",
         "rise": "Time of rise", 
         "az_rise": "Azimut at time of rise",
         "transit": "Time of transit", 
         "elevation_transit": "Elevation at time of transit", 
         "set": "Time of set", 
         "az_set": "Azimut at time of set",
         "twilight": {
            "civil": {
               "dawn": "Time of the morning civil twilight",
               "dusk": "Time of the evening civil twilight"
            },
            "nautical": {
               "dawn": "Time of the morning nautical twilight",
               "dusk": "Time of the evening nautical twilight"
            },
            "astronomical": {
               "dawn": "Time of the morning astronomical twilight",
               "dusk": "Time of the evening astronomical twilight"
            }
         },
         "duration_observability": "Duration of the observability window", 
         "magnitude": "Apparent visual magnitude",
         "RA": "Right ascension of the body at peak elevation",
         "DEC": "Declination of the body at peak elevation",
         "galatic_longitude": "Galatic longitude of the body at peak elevation",
         "galatic_latitude": "Galatic latitude of the body at peak elevation",
         "rate_of_motion": "Apparent rate of motion of the body at peak elevation",
         "angular_diameter": "Apparent angular diameter of the body at peak elevation",
         "helio_distance": "Distance between the body and the Sun",
         "obs_distance": "Distance between the body and the observer",
         "solar_elongation": "Solar elongation of the body at peak elevation",
         "lunar_elongation": "Lunar elongation of the body at peak elevation"
      },

      "unit": {
         "date": "ISO8601 date",
         "rise": "ISO8601 date", 
         "az_rise": "deg",
         "transit": "ISO8601 date", 
         "elevation_transit": "deg", 
         "set": "ISO8601 date", 
         "az_set": "deg",
         "twilight": {
            "civil": {
               "dawn": "ISO8601 date",
               "dusk": "ISO8601 date"
            },
            "nautical": {
               "dawn": "ISO8601 date",
               "dusk": "ISO8601 date"
            },
            "astronomical": {
               "dawn": "ISO8601 date",
               "dusk": "ISO8601 date"
            }
         },
         "duration_observability": "min", 
         "magnitude": "mag",
         "RA": "deg",
         "DEC": "deg",
         "galatic_longitude": "deg",
         "galatic_latitude": "deg",
         "rate_of_motion": "arcsec/h",
         "angular_diameter": "arcsec",
         "helio_distance": "au",
         "obs_distance": "au",
         "solar_elongation": "deg",
         "lunar_elongation": "deg"
      }
   }
}
