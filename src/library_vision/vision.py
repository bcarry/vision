import json
# import rocks
import query

from filter import (ElevationRequestFilter,MagnitudeRequestFilter, SolarElongationRequestFilter, LunarElongationRequestFilter,
 PhaseAngleRequestFilter, HeliocentricDistanceRequestFilter, RangeToObserverRequestFilter, AngularDiameterRequestFilter,
 DurationObservabilityRequestFilter, FilterDataRequest)

class TargetRequestData:
    def __init__(self, name):
        self.name = name

class LocationRequestData:
    def __init__(self, name, coordinates):
        self.name = name
        self.coordinates = coordinates

class Observation:
    def __init__(self, date, data):
        self.date = date
        self.data = data

    def get_target_data(self, target):
        return self.data.get(target, {})

    def get_all_data(self):
        return self.data


class JSONAnalyzer:
    def __init__(self, json_file_path):
        self.data = self.read_json(json_file_path)
        self.response = self.read_json(json_file_path)["response"]["data"]
        self.filters = self.extract_filters()
        self.targets = self.extract_target()
        self.response_by_date = self.create_observations()
        self.dates = self.list_dates()

    def read_json(self, json_file_path):
        with open(json_file_path, 'r') as file:
            data = json.load(file)
        return data

    def list_dates(self):
        # Create a list of requested dates
        epochs = []
        for date_data in self.data["response"]["data"]:
            for date, observations in date_data.items():
                epochs.append(date)
        return epochs

    def display_response(self):
        # Affichage des données de la réponse
        for date_data in self.data["response"]["data"]:
            for date, observations in date_data.items():
                print(f"Date: {date}")
                for target, details in observations.items():
                    print(f"\tTarget: {target}")
                    for key, value in details.items():
                        print(f"\t\t{key}: {value}")
    
    def extract_filters(self):
        filters_data = self.data["request"]["filters"]
        filters = {}
        for filter_name, filter_values in filters_data.items():
            if filter_name == "elevation":
                filters[filter_name] = ElevationRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "magnitude":
                filters[filter_name] = MagnitudeRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "solar_elongation":
                filters[filter_name] = SolarElongationRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "lunar_elongation":
                filters[filter_name] = LunarElongationRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "phase_angle":
                filters[filter_name] = PhaseAngleRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "heliocentric_distance":
                filters[filter_name] = HeliocentricDistanceRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "range_to_observer":
                filters[filter_name] = RangeToObserverRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "angular_diameter":
                filters[filter_name] = AngularDiameterRequestFilter(filter_values.get("min"), filter_values.get("max"))
            elif filter_name == "duration_observability":
                filters[filter_name] = DurationObservabilityRequestFilter(filter_values.get("min"), filter_values.get("max"))
            else:
                filters[filter_name] = FilterDataRequest(filter_values.get("min"), filter_values.get("max"))

        return filters
    
    def extract_target(self):
        targets_data = self.data["request"]["targets"]
        targets = {}
        for target_type, target_name in targets_data.items():
            print( target_type, target_name)
            # targets[target_type] = TargetRequestData(target_name)
            
            targets[target_type] = []

            match target_type:
                case "planet":
                    for i, t in enumerate(target_name):
                        targets[target_type].append(
                            query.Target( type='planet', ssodnet=t )
                        )
                case "asteroid":
                    for i, t in enumerate(target_name):
                        targets[target_type].append(
                            query.Target( type='asteroid', ssodnet=t )
                        )                        
                case "simbad":
                    for i, t in enumerate(target_name):
                        targets[target_type].append(
                            query.Target( cds=t )
                        )                

            # targets[target_type] = target_name
        
        return targets
    
    def extract_results_by_date(self):
        results_by_date = {}

        for item in self.response:
            for date, results in item.items():
                if date not in results_by_date:
                    results_by_date[date] = {}

                for target, data in results.items():
                    if target not in results_by_date[date]:
                        results_by_date[date][target] = {}

                    for key, value in data.items():
                        results_by_date[date][target][key] = value

        return results_by_date
    
    def create_observations(self):
        results_by_date = self.extract_results_by_date()
        observations = []

        for date, data in results_by_date.items():
            observation = Observation(date, data)
            observations.append(observation)

        return observations
    
    

    

if __name__ == "__main__":

    # print("=======================================================================================================================")
    # print("=============================================CREATE OBJECT=============================================================")
    # print("=======================================================================================================================")
  
    # createObject = JSONAnalyzer("../../data/ephevision.json")

    # print("=======================================================================================================================")
    # print("=============================================Display JSON=============================================================")
    # print("=======================================================================================================================")


    # print("execute function display_response() of the object")
    # createObject.display_response()

    # print("=======================================================================================================================")
    # print("=============================================Print Attr Obj=============================================================")
    # print("=======================================================================================================================")


    # print("print data")
    # print(createObject.data)

    # print("print response")
    # print(createObject.response)

    # print ("filter")
    # print(createObject.filters)

    # print("=======================================================================================================================")
    # print("=============================================Print Filter =============================================================")
    # print("=======================================================================================================================")


    # print("filter elevation")
    # print(createObject.filters["elevation"])

    # print("min value of elevation filter")
    # print(createObject.filters["elevation"].min_value)

    # print("set a new value for min value of elevation filter")
    # createObject.filters["elevation"].set_min_value(8.0)

    # print("print again min value of elevation filter after modification")
    # print(createObject.filters["elevation"].min_value)

    # print("print min and max value for solar elongation")
    # print(createObject.filters["solar_elongation"].min_value)
    # print(createObject.filters["solar_elongation"].max_value)

    # print("=======================================================================================================================")
    # print("=============================================Print Targets=============================================================")
    # print("=======================================================================================================================")


    # print("print target")
    # print(createObject.targets)
    # print(createObject.targets["asteroid"].name)

    # print("=======================================================================================================================")
    # print("=============================================print response by date=============================================================")
    # print("=======================================================================================================================")


    # print(createObject.response_by_date)

    # print("=======================================================================================================================")
    # print("========================================print value for a date=========================================================")
    # print("=======================================================================================================================")


    # print("=======================================================================================================================")
    # print("=============================================Use getter================================================================")
    # print("=======================================================================================================================")

    # print(createObject.response_by_date[0].get_all_data())

    # print("=======================================================================================================================")
    # print("=========================================Use getter for sun============================================================")
    # print("=======================================================================================================================")
    # print(createObject.response_by_date[0].get_target_data("Ceres"))


    view = JSONAnalyzer("../../data/ephevision.json")
    print(view.dates)
    print(view.targets)
    # print(view.targets['asteroid'])

    for i, a in enumerate(view.targets['simbad']):
        print( i, a.type, a.number, a.name, a.coordinates)

    for i, a in enumerate(view.targets['asteroid']):
        print( i, a.type, a.number, a.name, a.coordinates)

    for i, a in enumerate(view.targets['planet']):
        print( i, a.type, a.number, a.name, a.coordinates)

