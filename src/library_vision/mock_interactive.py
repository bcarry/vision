from vision import Vision

# --------------------------------------------------------------------------------
# 1. Create query
# Make a general query
vision_req = Vision(
    target=["a:Ceres", "c:67", "p:Saturn"],
    epochs=["2025-01-01", "2025-01-15", "2025-02-01"],
    location={"longitude":"+07.1234", "latitude": "+45.4325", "elevation": "1250"},
)
# vision_req could be an object with just the "request" part, but it may be too complex.
# it could be the same Class as view: a complete "vision" object corresponding to the full json response (request, response, description)

# Add some targets
vision_req.targets.add(planet="Jupiter")
vision_req.targets.add("eq:280.0,+32.34")
vision_req.targets.add(simbad=["M_31", "HIP_11767"])

# getter/setter are not super pythonic
#  - target class "Target" with add/remove methods?
#  - attributes of Target
#    - type: str (asteroid, comet, planet, coordinate, simbad)
#    - name: str
#    - label: str
#    - **kwargs: number (if asteroids, planets, comets, etc)

# Print location
print(vision_req.location)

# Change the location
vision_req.location.set( {"iaucode":"010"} )

# Similar comment location
#  - location class "Location" with @property syntax?
#  - attributes of Location
#    - longitude: float
#    - latitude: float
#    - elevation: float
#    - label: str
#    - iaucode: str
#    - **kwargs: horizon, timezone, etc

# Set a filter (all filters are created at default value anyway)
vision_req.filters.elevation.min = 25.0
vision_req.filters.elevation.max = 90.0

# Print all targets that are planets 
print(vision_req.targets["planet"])

# Remove a target
vision_req.targets.remove(planet="Saturn")

# Save query for later usage
vision_req.to_json("my_query.json")


# Thoughts
#  - class QueryParameter ?
#     - init, default, set, get
#     - childs: QueryTarget, QueryFilter, QueryLocation, QueryOption ?





# --------------------------------------------------------------------------------
# 2. Get response from API
view = vision_req.query()
# view should be a vision complete object corresponding to the full json response (request, response, description)

# save for later usage
view.to_json("file.json")
# alternative, load from file
view = Vision.from_json("file.json")

# NB, one should also be able to query from the full object:
view = view.query()
# Or directly?
view.query()




# --------------------------------------------------------------------------------
# 3. Check observability
view.check_observability()
# for each night each objet, test if observable, add that info to the object
# it's a mere wrapper to do something like:

import astropy.units as u
from astropy.coordinates import (SkyCoord, AltAz, EarthLocation)
from astropy.time import Time

# Define astropy observer object from vision.location object
observing_location = EarthLocation.from_geodetic(
    lon=view.location.longitude*u.deg, lat=view.location.latitude*u.deg)

for date in view.dates:

    observing_date = Time(date)
    time_grid = observing_date + np.linspace(0, 24, 288) * u.hour
    altaz = AltAz(location=observing_location, obstime=time_grid)

    for target in view.targets:
        if target.is_observable(date, AltAz):
            target.observability[date] = True

            # Or?
            target[date].observability = True
            

# --------------------------------------------------------------------------------
# 4. Create plots (opt)
view.plot()  # for each night with more than one object observable, make a plot (window)
view.plot("2025-01-15")  # plot just that night

# Or is this better?
view("2025-01-15").plot()
# as we could imagine
view("2025-01-15")
# being the way to get everything from that night


view.savefig(format="png")  # create/save all figures to disk (png) (default outdir="cache/")
view.savefig("2025-01-15", outdir="./", format="png")  # create/save a single figures to disk (png)
view.savefig(outdir="PATH_TO_DIR", format="pgf")  # create/save all figures to disk (pgf)

# (opt) change filters / redo checks etc
view.filters.elevation.min = 30.0
view.filters.magnitude.min = 10.0
view.filters.magnitude.max = 15.0
view.check_observability()
view.plot()

# --------------------------------------------------------------------------------
# 5. Export results(opt)
view.to_pdf("file.pdf") # default outdir="cache/", figures=True by default = create them for compilation
view.to_md("file.md", figures=False) # do not create figures
view.to_html(outdir="PATH_TO_DIR", indir='PATH_TO_INPUT', "file.pdf") # default indir="cache/"
view.to_votable("file.xml") # figure=False by default for VOTable
