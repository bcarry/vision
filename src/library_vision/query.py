# Class for selection filters
class Filter:

    def __init__(self, min_value=None, max_value=None):
        self.min = min_value
        self.max = max_value

    @property
    def min(self):
        return self._min

    @min.setter
    def min(self, value):
        self._min = value

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, value):
        self._max = value


# Class for Targets
class Target:

    def __init__(
        self,
        type=None,
        name=None,
        label=None,
        number=None,
        coordinates=None,
        cds=None,
        ssodnet=None,
    ):

        # Default
        self.type = type
        self.number = number
        self.name = name
        self.label = label
        self.coordinates = coordinates
        self.visible = False

        # Resolve object from CDS/SIMBAD
        if cds is not None:
            from astroquery.simbad import Simbad

            result = Simbad.query_object(cds)

            if not result is None:
                from astropy.coordinates import SkyCoord
                import astropy.units as u

                self.type = "CDS"
                self.name = cds
                self.coordinates = SkyCoord(
                    ra=result["RA"], dec=result["DEC"], unit=(u.hourangle, u.deg)
                )

                return

        # Resolve object from SSODNet
        if ssodnet is not None:
            import rocks
            import numpy as np

            if type is None:
                self.type = "SSO"

            match self.type:
                case "asteroid":
                    nam_num = rocks.identify(ssodnet)

                    if nam_num != (None, np.nan):
                        self.number = nam_num[1]
                        self.name = nam_num[0]
                        # self.type = "SSO" # to be replaced by sso_type
                        self.label = f"{ssodnet}"

                    return
                
                case "planet":
                    url = "https://api.ssodnet.imcce.fr/quaero/1/sso?"
                    import requests

                    params = {"q": ssodnet, "type": "planet"}
                    try:
                        r = requests.post(url, params=params, timeout=50)
                    except requests.exceptions.ReadTimeout:
                        return False


    j = r.json()


        # Convert input coordinates into SkyCoord
        if coordinates is not None:
            from astropy.coordinates import SkyCoord

            if isinstance(coordinates, str):
                import astropy.units as u
                self.coordinates = SkyCoord(coordinates, unit=(u.hour, u.deg))
                return
            
            elif isinstance(coordinates, SkyCoord):
                self.coordinates = coordinates
                return

    # --------------------------------------------------------------------------------
    # Getters
    @property
    def type(self):
        return self._type

    @property
    def number(self):
        return self._number

    @property
    def name(self):
        return self._name

    @property
    def label(self):
        return self._label

    @property
    def coordinates(self):
        return self._coordinates

    # --------------------------------------------------------------------------------
    # Setters
    @type.setter
    def type(self, value):
        self._type = value

    @number.setter
    def number(self, value):
        self._number = value

    @name.setter
    def name(self, value):
        self._name = value

    @label.setter
    def label(self, value):
        self._label = value

    @coordinates.setter
    def coordinates(self, value):
        self._coordinates = value

    # --------------------------------------------------------------------------------
    # Resolver methods