class FilterDataRequest:

    def __init__(self, min_value=None, max_value=None):
        self.min_value = min_value
        self.max_value = max_value
    
    def set_min_value(self, min_value):
        self.min_value = min_value
    
    def set_max_value(self, max_value):
        self.max_value = max_value
    
    def get_min_value(self):
        return self.min_value

    def get_max_value(self):
        return self.max_value

    def apply_filter(self, value):
        """
        Basic method for applying the filter

        Parameters
        ----------
        inputs : name (string),
        value : value of the object for a date and a target
        

        Returns
        -------
        outputs : Booleen
            True if filter pass or False if failed

            """
        if self.min_value is not None and value < self.min_value:
            return False
        if self.max_value is not None and value > self.max_value:
            return False
        return True
    
class ElevationRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class MagnitudeRequestFilter(FilterDataRequest):
    def __iniy__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class SolarElongationRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class LunarElongationRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class PhaseAngleRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class HeliocentricDistanceRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class RangeToObserverRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class AngularDiameterRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)

class DurationObservabilityRequestFilter(FilterDataRequest):
    def __init__(self, min_value=None, max_value=None):
        super().__init__(min_value, max_value)
