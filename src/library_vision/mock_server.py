import vision


# 0. wrapper (in the CLI?) to get :
#  - name of input file
#  - output directory
#  - mime? But present in the json...
in_file = "file.json"
out_file = "file.exten"
dir = "PATH_TO_DIR"

# 1. Load the created by the API
view = vision(in_file)


# 2. Check observability
view.check_observability()

# 3. Export results
match view.mime:
    case "pdf":
        view.to_pdf(out_file, outdir=dir)

    case "html":
        view.to_html(out_file, outdir=outdir)

    case "votable":
        view.to_votable(out_file, outdir=outdir)

    case "markdown":
        view.to_markdown(out_file, outdir=outdir)
