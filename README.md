# vision

# Installation for development

1. Clone the repository:

```git clone git@gitlab.oca.eu:bcarry/vision.git```


2. Create your environement:

```conda create -n YOUR_ENV_NAME```

3. Install your dependencies:

```conda install --file requirements-dev.txt```

# Main command

build: ```python -m build```

docs: ```pdoc --output-dir docs/ --html --force library_vision```

test:

    pytest

    OR
 
    pytest PATH_TO_FILE

coverage:

	coverage run -m pytest
    # Make the html version of the coverage results. 
	coverage html 


# Metadata

https://ssp.imcce.fr/webservices/miriade/api/vision/ephevision_metadata.json

https://ssp.imcce.fr/webservices/miriade/api/vision/ephevision_template.json

Sur asterm
> vision4F -j ephevision-Caussols-noon.json