\documentclass[10pt,svgnames,a4paper,twoside]{article}

%%%--- Margins ---%%%
\usepackage[
  margin=0cm,
  left=1cm, 
  right=1cm, 
  textwidth=19cm,
  top=1.25cm, 
  bottom=1.25cm, 
  headsep=2mm, 
  footskip=2mm]{geometry}

%%%--- PGF ---%%%
\usepackage{pgf}
\def\mathdefault#1{#1}
\everymath=\expandafter{\the\everymath\displaystyle}
\usepackage{amsmath} \usepackage[utf8x]{inputenc} \usepackage[T1]{fontenc} \usepackage{txfonts} \usepackage[default]{sourcesanspro} \usepackage{pgfplots} \usepgfplotslibrary{external} \tikzexternalize \usepackage{xcolor} 
\usepackage{fontspec}
\makeatletter\@ifpackageloaded{underscore}{}{\usepackage[strings]{underscore}}\makeatother

%%%--- Header & Footer ---%%%
\newcommand{\epoch}{~}
\usepackage{tcolorbox}
\usepackage{fancyhdr}
\fancyhead[L]{}
\fancyhead[C]{\begin{tcolorbox}[halign=center, colframe=blue, colback=blue!30, arc=2mm]
\textbf{\epoch}
\end{tcolorbox}
}
\fancyhead[R]{}
\fancyfoot[L]{
  \href{https://ssp.imcce.fr/webservices/miriade/api/vision/}{ViSiON} at
  \href{http://vo.imcce.fr/webservices/}{VO SSP}
  \href{https://ui.adsabs.harvard.edu/abs/2018P&SS..164...79C/abstract}{(Carry \& Berthier 2018)}}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.4pt}
\pagestyle{fancy}

%%%--- Hyperreferences ---%%%%
\usepackage{hyperref}
\hypersetup{
%%%%--- Options for Acrobat
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat's bookmarks
    pdftoolbar=true,        % show Acrobat's toolbar?
    pdfmenubar=true,        % show Acrobat's menu?
    pdffitwindow=false,     % page fit to window when opened
%%%%--- PDF informations
    pdftitle={ViSiON},
    pdfauthor={VO SSP, 2012-2025, B. Carry and J. Berthier}, 
    pdfsubject={Planetary Science Ephemeris}, 
    pdfkeywords={},         % list of keywords
%%%%--- Link option
    pdfnewwindow=true,      % links in new window
    colorlinks=true,        % false: boxed links; true: colored links
    linkcolor=gray,         % color of internal links
    citecolor=blue,         % color of links to bibliography
    filecolor=gray,         % color of file links
    urlcolor=gray           % color of external links
}

%%%--- Useful Commands ---%%%%
\newcommand{\degr}{\ensuremath{^{\circ}}}
\newcommand{\arcmin}{\mbox{\ensuremath{^{\prime}}}}
\newcommand{\arcsec}{\mbox{\ensuremath{^{\prime\prime}}}}
\renewcommand{\thefigure}{\arabic{figure}}

%%%--- Misc ---%%%%
\usepackage{multicol}

%%%--------------------------------------------------------------------------------
\begin{document}

  \vspace*{1.5cm}
  \thispagestyle{empty}

  %%%--- ViSiON Title ---%%%%
  \begin{center}
    \includegraphics[]{logo-vision}

    \vspace{1em}
    {\huge \textbf{Visibility charts from\\}}
    \vspace{0.5em}
    {\huge \textbf{OBSERVATORY}}
  \end{center}
  \vspace{1em}

%%%--- List of epochs ---%%%%
\section{List of epochs}
  
\begin{multicols}{3}
\input{list}
\end{multicols}

\section{Help}
\begin{multicols}{3}
  \begin{itemize}
  \item[$\triangleright$] \hyperlink{cuts}{Selection criteria for observability}
  \item[$\triangleright$] \hyperlink{key}{Meaning of quantities}
  \item[$\triangleright$] \hyperlink{credits}{Credits}
\end{itemize}
\end{multicols}



\input{epochs}

\clearpage

%%%--- Cuts description ---%%%%
~\vspace*{1em}
\hypertarget{cuts}{\section{Cuts applied to the selection}}
\begin{center}
  \begin{tabular}{llll}
    \hline
    Quantity & Condition & Description & Units\\
    \hline
    % \input{cuts}  
  \end{tabular}
\end{center}

%%%--- Columns Key ---%%%%
\hypertarget{key}{\section{Meaning of displayed quantities}}
\begin{center}
  \begin{tabular}{lll}
    \hline
    Quantity & Description & Units\\
    \hline
    Target              & Target designation                 & --       \\
    V                   & Apparent magnitude                 & mag      \\
    $\mathcal{D}$       & Duration of visibility window      & h:m      \\
    RA                  & Right Ascension                    & h:m:s    \\
    DEC                 & Declination                        & d:m:s    \\
    Long.               & Galactic longitude                 & deg      \\
    Lat.                & Galactic latitude                  & deg      \\
    Alt.                & Altitude                           & deg      \\
    Az                  & Azimuth                            & deg      \\
    $\widehat{\rm SEO}$ & Solar elongation                   & deg      \\
    $\widehat{\rm MEO}$ & Moon elongation                    & deg      \\
    $r$                 & Range to observer                  & au       \\
    $\Delta$            & Heliocentric distance              & au       \\
    $\alpha$            & Solar phase angle                  & deg      \\
    Rate                & Apparent non-sidereal motion       & arcsec/h \\
    \hline
  \end{tabular}\\
  \vspace{1em}
  For each target, the values are reported at the time of the highest
  altitude.
\end{center}



%%%--- Credits ---%%%%
\hypertarget{credits}{\section*{Credits}}

\textbf{ViSiON}
(\textbf{Vi}sibility
\textbf{S}erv\textbf{i}ce
for
\textbf{O}bserving
\textbf{N}ights) has been developed by
Benoit Carry
and
J{\'e}r{\^o}me Berthier 
at Laboratoire Lagrange of the Observatoire de la C{\^o}te d'Azur and
Laboratoire Temps-Espace (LTE) of Observatoire de Paris.\\

\noindent If ViSiON was helful for your research, please 
cite \href{https://ui.adsabs.harvard.edu/abs/2018P&SS..164...79C/abstract}{Carry \& Berthier (2018)}
add the following 
in your acknowledgments: ``This publication makes use of the
Virtual Observatory Web service ViSiON, developed by LTE and OCA.''\\

\noindent To contact us, please email \href{mailto:vo.imcce@obspm.fr?subject=ViSiON}{vo.imcce@obspm.fr}

\begin{tcolorbox}[halign=center, colframe=DeepSkyBlue, colback=DeepSkyBlue!30, arc=2mm]
\begin{verbatim}
@ARTICLE{2018P\&SS..164...79C,
       author = {{Carry}, B. and {Berthier}, J.},
        title = "{ViSiON: Visibility Service for Observing Nights}",
      journal = {\\planss},
         year = 2018,
        month = dec,
       volume = {164},
        pages = {79-84},
          doi = {10.1016/j.pss.2018.06.012},
archivePrefix = {arXiv},
       eprint = {1806.09425},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2018P\&SS..164...79C},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
\end{verbatim}
\end{tcolorbox}

% %%-- Be sure we start on a ``left'' page
%   \checkoddpage
%   \ifthenelse{\boolean{oddpage}}{%
%     \newpage
%     \thispagestyle{empty}
%     \mbox{}
%   }{}
%   \cleardoublepage 

%   % \input{load}



\end{document}


