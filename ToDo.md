# ViSiON Development Plan

## Target class
- [ ] Resolver methods
- [ ] Update method
- [ ] is_visible?

## Epoch class
- [ ] Wrapper to N Target.is_visible
- [ ] PlotSky
- [ ] PlotTime
- [ ] RTS class?

## Vision class
- [ ] build summary

## Core
- [ ] FloatValue object a la rocks?